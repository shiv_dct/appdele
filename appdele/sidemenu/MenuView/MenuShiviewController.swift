//
//  MenuShiviewController.swift
//  appdele
//
//  Created by Danish computer technologies on 24/02/22.
//

import UIKit

protocol SideMenuViewControllerDelegate {
    func selectedCell(_ row: Int)
}

class ShivMenuViewController: UIViewController {
    
    var safeArea: UILayoutGuide!
    
   
  /*
//    @IBOutlet var headerImageView: UIImageView!
    let headerImageView:UIImageView = {
             let img = UIImageView()
        img.backgroundColor = .blue//Constants.Design.Color.Primary.v3Lightblue
             img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
             img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
             img.layer.cornerRadius = 17.5
             img.image = UIImage(systemName: "house.fill")
             img.clipsToBounds = true
       
            return img
         }()
//    @IBOutlet var sideMenuTableView: UITableView!*/
    fileprivate let sideMenuTableView:UITableView = {
   
        let tv = UITableView(frame: .zero,style: .grouped)
        tv.showsVerticalScrollIndicator = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.allowsMultipleSelection = true
       
        tv.register(SideMenuCell.self, forCellReuseIdentifier: SideMenuCell.identifier)
        tv.backgroundColor = .clear
        
        
        tv.separatorStyle = .none
        //tv.keyboardDismissMode = .onDrag
        
        return tv
    }()
//    @IBOutlet var footerLabel: UILabel!
    let footerLabel:UILabel = {
            let label = PaddingLabel()
        //label.backgroundColor = .purple//Constants.Design.Color.Primary.v3txtfFontCol
       // label.font = Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        //label.text = "Reminder set"
        label.numberOfLines = 1
        label.leftInset = 25
       // label.sizeToFit()
            return label
    }()

    var delegate: SideMenuViewControllerDelegate?

    var defaultHighlightedCell: Int = 0

    var menuGuest: [ShivMenuModel] = [
        ShivMenuModel(icon: UIImage(named: "sidehome")!, title: "Home"),
        ShivMenuModel(icon: UIImage(named: "sideterms")!, title: "Terms & Conditions"),
        ShivMenuModel(icon: UIImage(named: "sideabout")!, title: "About"),
        ShivMenuModel(icon: UIImage(named: "siderate")!, title: "Rate App"),
        ShivMenuModel(icon: UIImage(named: "sideshare")!, title: "Share App"),
       // ShivMenuModel(icon: UIImage(systemName: "slider.horizontal.3")!, title: "Settings"),
       // ShivMenuModel(icon: UIImage(systemName: "hand.thumbsup.fill")!, title: "Like us on facebook")
    ]

    override func loadView() {
      super.loadView()
        view.backgroundColor = .black//secondarySystemBackground//Constants.Design.Color.Primary.v3superbackgroundCol
      safeArea = view.layoutMarginsGuide
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //view.backgroundColor = .yellow
        // TableView
        self.sideMenuTableView.delegate = self
        self.sideMenuTableView.dataSource = self
       // self.sideMenuTableView.backgroundColor = #colorLiteral(red: 0.737254902, green: 0.1294117647, blue: 0.2941176471, alpha: 1)
        self.sideMenuTableView.separatorStyle = .none
        // add constraint
        view.addSubview(sideMenuTableView)
        view.addSubview(footerLabel)
        sideMenuTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        sideMenuTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        sideMenuTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        sideMenuTableView.bottomAnchor.constraint(equalTo: self.safeArea.bottomAnchor, constant: -70).isActive = true
        footerLabel.leadingAnchor.constraint(equalTo: sideMenuTableView.leadingAnchor).isActive = true
        footerLabel.trailingAnchor.constraint(equalTo: sideMenuTableView.trailingAnchor).isActive = true
        footerLabel.topAnchor.constraint(equalTo: sideMenuTableView.bottomAnchor,constant: 16).isActive = true
        footerLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        // Set Highlighted Cell
        DispatchQueue.main.async {
            let defaultRow = IndexPath(row: self.defaultHighlightedCell, section: 0)
            self.sideMenuTableView.selectRow(at: defaultRow, animated: false, scrollPosition: .none)
        }

        // Footer
        self.footerLabel.textColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)//.black
        self.footerLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 18.0)//UIFont.systemFont(ofSize: 12, weight: .bold)
        let userDefaults = UserDefaults.standard
        if userDefaults.value(forKey: "login") != nil{ self.footerLabel.text = "Log Out"}else{
            self.footerLabel.text = "Login Here"
        }
       
    
        let tap = UITapGestureRecognizer(target: self, action:#selector(logoutAction))
        footerLabel.addGestureRecognizer(tap)
        footerLabel.isUserInteractionEnabled = true
        
        
        
        // Set Highlighted Cell
               DispatchQueue.main.async {
                   let defaultRow = IndexPath(row: self.defaultHighlightedCell, section: 0)
                   self.sideMenuTableView.selectRow(at: defaultRow, animated: false, scrollPosition: .none)
               }

        
        
        
        // Register TableView Cell
     //self.sideMenuTableView.register(SideMenuCell.nib, forCellReuseIdentifier: SideMenuCell.identifier)

        // Update TableView with the data
        self.sideMenuTableView.reloadData()
    }
    
    @objc func logoutAction(){
       
        if footerLabel.text == "Login Here"{
            self.delegate?.selectedCell(101)// login
        }else{
            self.delegate?.selectedCell(102)//logout
        }
       
        
    }
    
}

// MARK: - UITableViewDelegate

extension ShivMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

// MARK: - UITableViewDataSource

extension ShivMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuGuest.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.identifier, for: indexPath) as? SideMenuCell else { fatalError("xib doesn't exist") }

        cell.iconImageView.image = self.menuGuest[indexPath.row].icon
        cell.titleLabel.text = self.menuGuest[indexPath.row].title

        // Highlighted color
//        let myCustomSelectionColorView = UIView()
//        myCustomSelectionColorView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        cell.selectedBackgroundView = myCustomSelectionColorView
        return cell
        //return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedCell(indexPath.row)
        tableView.deselectRow(at: IndexPath(row: defaultHighlightedCell, section: 0), animated: true)
        defaultHighlightedCell = indexPath.row
        /*
        // Remove highlighted color when you press the 'Profile' and 'Like us on facebook' cell
        if indexPath.row == 4 || indexPath.row == 6 {
            tableView.deselectRow(at: indexPath, animated: true)
        }
         */
    }
}

class PaddingLabel: UILabel {

   @IBInspectable var topInset: CGFloat = 5.0
   @IBInspectable var bottomInset: CGFloat = 5.0
   @IBInspectable var leftInset: CGFloat = 5.0
   @IBInspectable var rightInset: CGFloat = 5.0

   override func drawText(in rect: CGRect) {
      let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
       super.drawText(in: rect.inset(by: insets))
   }

   override var intrinsicContentSize: CGSize {
      get {
         var contentSize = super.intrinsicContentSize
         contentSize.height += topInset + bottomInset
         contentSize.width += leftInset + rightInset
         return contentSize
      }
   }
}

extension UIApplication {
    
    var keyWindow: UIWindow? {
        // Get connected scenes
        return UIApplication.shared.connectedScenes
            // Keep only active scenes, onscreen and visible to the user
            .filter { $0.activationState == .foregroundActive }
            // Keep only the first `UIWindowScene`
            .first(where: { $0 is UIWindowScene })
            // Get its associated windows
            .flatMap({ $0 as? UIWindowScene })?.windows
            // Finally, keep only the key window
            .first(where: \.isKeyWindow)
    }
    
}
