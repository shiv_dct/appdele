//
//  MenuCell.swift
//  appdele
//
//  Created by Danish computer technologies on 24/02/22.
//

import UIKit
class SideMenuCell: UITableViewCell {
    //TaskCell default for all over
    static let identifier = "SideMenuCell"
    
//    override var isSelected: Bool{
//        didSet{
//            titleLabel.textColor = isSelected ? .red : .white
//        }
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           if selected {
               contentView.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
               titleLabel.textColor = UIColor.black
               iconImageView.tintColor = .black
               
           } else {
               contentView.backgroundColor = UIColor.clear
               titleLabel.textColor = UIColor.white
               iconImageView.tintColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
           }
       }

   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = nil
        backgroundColor = .clear
        selectionStyle = .none
        //self.contentView.addSubview(containerView)
      
        
//        containerView.topAnchor.constraint(equalTo:self.contentView.topAnchor,constant: 2).isActive = true
//        containerView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:16).isActive = true
//        containerView.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant:-16).isActive = true
//        containerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor,constant: -4).isActive = true
        
        addSubview(titleLabel)
        addSubview(iconImageView)
      //  containerView.addSubview(subLabel)
      //  containerView.addSubview(btn)
        
        iconImageView.leadingAnchor.constraint(equalTo:contentView.leadingAnchor, constant:8).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 36).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant:36).isActive = true
        iconImageView.topAnchor.constraint(equalTo:contentView.topAnchor,constant: 4).isActive = true
        iconImageView.bottomAnchor.constraint(equalTo:contentView.bottomAnchor,constant: -4).isActive = true
        //nameLabel.bottomAnchor.constraint(equalTo:containerView.bottomAnchor,constant: -4).isActive = true
       // titleLabel.trailingAnchor.constraint(equalTo:containerView.trailingAnchor, constant:-50).isActive = true
    
        
        
//        btn.centerYAnchor.constraint(equalTo:containerView.centerYAnchor).isActive = true
//        btn.trailingAnchor.constraint(equalTo:containerView.trailingAnchor, constant:-6).isActive = true
//        btn.widthAnchor.constraint(equalToConstant: 40).isActive = true
//        btn.heightAnchor.constraint(equalToConstant:40).isActive = true
//
//        subLabel.leadingAnchor.constraint(equalTo:nameLabel.leadingAnchor, constant:16).isActive = true
//        subLabel.trailingAnchor.constraint(equalTo: nameLabel.trailingAnchor).isActive = true
//        subLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
//
//        subLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16).isActive = true
    
        titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
      
       // containerView.setROundCornerAndShadowArround()
    }

     required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    /*
    let containerView:UIView = {
      let view = UIView()
        view.backgroundColor = .white
//        view.layer.cornerRadius = 8
      view.translatesAutoresizingMaskIntoConstraints = false
      view.clipsToBounds = true // this will make sure its children do not go out of the boundary
      return view
    }()
    */
    let iconImageView:UIImageView = {
             let img = UIImageView()
       // img.backgroundColor = .green//Constants.Design.Color.Primary.v3Lightblue
             img.contentMode = .center // image will never be strecthed vertially or horizontally
             img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
             //img.layer.cornerRadius = 5
             img.clipsToBounds = true
        img.tintColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)//.black//
            return img
         }()
    
    let titleLabel:UILabel = {
            let label = UILabel()
        label.font = UIFont(name: "AppleSDGothicNeo-SemiBold", size: 14.0)//UIFont.systemFont(ofSize: 12, weight: .bold)
        label.textColor =  .white//tertiaryLabel//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.numberOfLines = 0
        label.sizeToFit()
            return label
    }()
    /*
    // chat button
    let btn:UIButton = {
        let b = UIButton(type: .custom)
        //b.setTitle("More", for: .normal)
       // b.setTitleColor(.gray, for: .normal)
       // b.titleLabel?.font = UIFont(name: "Helvetica", size: 16)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "logov3"), for: .normal)
        b.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //b.backgroundColor = .yellow
        return b
    }()
    *
    // subtask label
    let iconImageView:UILabel = {
            let label = UILabel()
        label.text = ""
        label.font = Constants.Design.Font.v3RMAN12//UIFont.boldSystemFont(ofSize: 20)
        label.textColor =  Constants.Design.Color.Primary.v3txtfFontLightCol
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
    }()
    
   var task:Task? {
            didSet {
                guard let data = task else {
                    nameLabel.text = "No result found."
                    return}
                nameLabel.text = data.name
                let subTasks = DBManager.sharedInstance.getSubTasks(id: data.taskId!)
                       subLabel.text = "Sub task : \(subTasks.count)"
            }
        }*/
}
