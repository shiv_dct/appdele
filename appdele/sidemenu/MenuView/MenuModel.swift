//
//  MenuModel.swift
//  appdele
//
//  Created by Danish computer technologies on 24/02/22.
//

import UIKit

struct ShivMenuModel {
    var icon: UIImage
    var title: String
}
