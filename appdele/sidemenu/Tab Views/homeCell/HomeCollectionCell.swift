//
//  HomeCollectionCell.swift
//  appdele
//
//  Created by Danish computer technologies on 08/03/22.
//
import UIKit
class BaseClass: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundView = UIView(frame: frame)
        setupSubViews()
        
    }
    var shouldTintBackgroundWhenSelected = true
//    override var isHighlighted: Bool {
//            didSet {
//             if shouldTintBackgroundWhenSelected   {
//                UIView.animate(withDuration: 0.5) {
//                    self.backgroundView?.backgroundColor = self.isHighlighted ? UIColor(white: 0.0, alpha: 0.2/*red: 0.929, green: 0.917, blue: 0.933, alpha: 1.0*/) : nil
//                }}
//
//            }
//        }
    required init?(coder: NSCoder) {
        super.init(coder: coder) // for interface builder
        //fatalError("init(coder:) has not been implemented")
    }
    func setupSubViews(){
        backgroundColor = .clear
    }
    
    /*override var isSelected: Bool {
        didSet {
            self.layer.borderWidth = 3.0
            self.layer.borderColor = isSelected ? UIColor.yellow.cgColor : UIColor.clear.cgColor
        }
    }*/
/*override var isHighlighted: Bool{
        didSet {
            //self.layer.borderWidth = 3.0
           // self.layer.borderColor = isHighlighted ? UIColor.yellow.cgColor : UIColor.clear.cgColor
        }
    }*/
}

class BannerCell:BaseClass{
    
  static let cellId = "Banner"
    
    let bannerImg:UIImageView = {
        let imagev = UIImageView()
        imagev.translatesAutoresizingMaskIntoConstraints = false
        imagev.layer.cornerRadius = 8
        imagev.backgroundColor = .white
       
        return imagev
    }()
   


    override func setupSubViews() {
        super.setupSubViews()
        backgroundColor = .clear
     
        shouldTintBackgroundWhenSelected = false
        
        addSubview(bannerImg)
        bannerImg.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        bannerImg.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        bannerImg.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        bannerImg.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
       
        contentView.layer.cornerRadius = 6.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true

        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 1, height: 0.5)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
        layer.backgroundColor = UIColor.clear.cgColor
       
       
    }

    
}
class ServiceCell:BaseClass{
   
    static let cellId = "Service"
    
    let backView:UIView = {
       let v = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 6
        v.layer.masksToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false

        return v
    }()
    
    let label:UILabel = {
           let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 14.0)
      
           label.sizeToFit()
           label.numberOfLines = 0
           return label
       }()
   
    let catImg:UIImageView = {
        let imagev = UIImageView()
        imagev.translatesAutoresizingMaskIntoConstraints = false
        //imagev.backgroundColor = .gray
        return imagev
    }()
   
     let forImg:UIImageView = {
         let imagev = UIImageView()
         //imagev.backgroundColor = .yellow
         imagev.translatesAutoresizingMaskIntoConstraints = false
         //imagev.image = #imageLiteral(resourceName: "imgAttach")
         imagev.contentMode = .scaleAspectFit
         imagev.layer.cornerRadius = 10
//         imagev.clipsToBounds = true
         return imagev
     }()
    
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        shouldTintBackgroundWhenSelected = false
        
        addSubview(backView)
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        backView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        backView.addSubview(catImg)
        backView.addSubview(label)
        backView.addSubview(forImg)
        
        catImg.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 8).isActive = true
        catImg.centerYAnchor.constraint(equalTo: backView.centerYAnchor).isActive = true
        catImg.widthAnchor.constraint(equalTo: backView.widthAnchor,multiplier: 0.25).isActive = true
        catImg.heightAnchor.constraint(equalTo: catImg.widthAnchor).isActive = true
        
        forImg.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: -8).isActive = true
        forImg.centerYAnchor.constraint(equalTo: catImg.centerYAnchor).isActive = true
        forImg.widthAnchor.constraint(equalToConstant: 20).isActive = true
        forImg.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        label.leadingAnchor.constraint(equalTo: catImg.trailingAnchor,constant: 8).isActive = true
        label.trailingAnchor.constraint(equalTo: forImg.leadingAnchor,constant: 8).isActive = true
        label.centerYAnchor.constraint(equalTo: catImg.centerYAnchor).isActive = true
        contentView.layer.cornerRadius = 6.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true

        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 1, height: 0.5)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
        layer.backgroundColor = UIColor.clear.cgColor
    
    }
    /*override func draw(_ rect: CGRect) {
       /* contentView.layer.cornerRadius = 10
        //contentView.layer.borderWidth = 0.0

       // contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true

        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 1, height: 1.0)
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect:bounds, cornerRadius:contentView.layer.cornerRadius).cgPath*/
       
    }*/
}

class CategoryCell:BaseClass{
   
    static let cellId = "Category"
        
    let label:UILabel = {
           let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "AppleSDGothicNeo-SemiBold", size: 14.0)
        //label.text = "testvnvbkj jkb kjbdsvljbv vkjbv "
           label.sizeToFit()
        label.textAlignment = .center
           label.numberOfLines = 4
           return label
       }()
   
    let catImg:UIImageView = {
        let imagev = UIImageView()
        imagev.translatesAutoresizingMaskIntoConstraints = false
        imagev.contentMode = .center//scaleAspectFit
        imagev.clipsToBounds = true
        //imagev.backgroundColor = .yellow
        return imagev
    }()
    let backView:UIView = {
       let v = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 6
        v.layer.masksToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false

        return v
    }()
  
    
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        shouldTintBackgroundWhenSelected = false
        
        
        addSubview(backView)
        backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 4).isActive = true
        backView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -4).isActive = true
        backView.topAnchor.constraint(equalTo: self.topAnchor,constant: 4).isActive = true
        backView.heightAnchor.constraint(equalTo: backView.widthAnchor).isActive = true
        
        backView.addSubview(catImg)
        catImg.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: 4).isActive = true
        catImg.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: 4).isActive = true
        catImg.topAnchor.constraint(equalTo: backView.topAnchor,constant: 4).isActive = true
        catImg.bottomAnchor.constraint(equalTo: backView.bottomAnchor,constant: -4).isActive = true
        addSubview(label)
        label.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 8).isActive = true
        label.trailingAnchor.constraint(equalTo: backView.trailingAnchor,constant: -8).isActive = true
        label.topAnchor.constraint(equalTo: backView.bottomAnchor,constant: 12).isActive = true
       
        
    }
    override func draw(_ rect: CGRect) {
       
        backView.clipsToBounds = false
        backView.layer.shadowColor = UIColor.lightGray.cgColor
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 1, height: 0.5)
        backView.layer.shadowRadius = 2.0
        backView.layer.shadowPath = UIBezierPath(roundedRect: backView.bounds, cornerRadius: backView.layer.cornerRadius).cgPath
    }
}

//    let nameLabel:UILabel = {
//        let label = UILabel()
//        label.font = UIFont(name: "Helvetica", size: 11)
//        label.textColor = .gray
//        //label.text = "chandra mohan sahu"
//        label.sizeToFit()
//        return label
//    }()
//    let resendBtn:MyButton = {
//        let button = MyButton()
//        button.backgroundColor = .white
//        button.setImage(UIImage(systemName: "memories"), for: .normal)
//        button.layer.cornerRadius = 15
//        button.translatesAutoresizingMaskIntoConstraints = false
//        return button
//    }()
/*

class MyButton: UIButton{

    var myRow: Int = 0
    var mySection: Int = 0

}*/

/*
class ImageChatCell: BaseClass {
    var msgI:MessagesR? {
        didSet{
            
            guard let m = msgI else {return}
            ///------//////
            
           // img.image = #imageLiteral(resourceName: "imgAttach")
           
           
           
          
            playbtn.isHidden = true
            if m.sentBy != DBManager.sharedInstance.getUser()?.userId!{
                textBubleView.frame = CGRect(x: 16, y: 14, width: 145, height: 145)
                resendBtn.isHidden = true
               // cell.addReSndButton(show: false)
               
               
                //cell.resendBtn.removeFromSuperview()
                //let strtag = "\(indexPath.section)\(indexPath.item)"
                if m.localPath != ""{
               // print(msg)
                                   let str = m.fileName
                    
                    if let imageurl = UIImage(contentsOfFile: V3ChatHelper.showImageTaskProfile(name:str, atPath: Medias.ATTAC_Chat_ReceiveIMAGE).path){
                        img.image = imageurl.scaleToSize(aSize: CGSize(width: 145, height: 145))
                        blurView.isHidden = true
                        Downloadbtn.isHidden = true
                        showbtn.isHidden = false
//                    showbtn.myRow = indexpath.item
//                    showbtn.mySection = indexpath.section
//                    showbtn.addTarget(self, action: #selector(pressShow(_:)), for: .touchUpInside)
                    }else{
                        img.sd_setImage(with: URL(string: m.serverPath), placeholderImage: UIImage(named: "radioOn"))
                        blurView.isHidden = false
                        Downloadbtn.isHidden = false
                        showbtn.isHidden = true
//                        Downloadbtn.myRow = indexpath.item
//                        Downloadbtn.mySection = indexpath.section
//                        Downloadbtn.addTarget(self, action: #selector(pressdownload(_:)), for: .touchUpInside)
                    }
                   
                    
               }else{

                    //let url = URL(string: msg.serverPath)

                    img.sd_setImage(with: URL(string: m.serverPath), placeholderImage: UIImage(named: "radioOn"))
                    blurView.isHidden = false
                    Downloadbtn.isHidden = false
                    showbtn.isHidden = true
                   // cell.Downloadbtn.tag = (strtag as NSString).integerValue
                    //cell.img.image = self.bluredImage(image: cell.img.image!, blurAmount: 10.0)
//                    cell.Downloadbtn.myRow = indexpath.item
//                    cell.Downloadbtn.mySection = indexpath.section
//                    cell.Downloadbtn.addTarget(self, action: #selector(pressdownload(_:)), for: .touchUpInside)

                }
                
                   timeLabel.text =  "\(m.date.toStringTime())"
                   timeLabel.textAlignment = .left
                   timeLabel.frame = CGRect(x: 12, y: textBubleView.frame.maxY, width: 70, height: 16)
                let viewModel = ViewModel()
                   let name = viewModel.returnUserName(contactnumber: [m.sentBy])
                   nameLabel.text = "\(name.first?.title ?? "\(m.sentBy)")"
                sizeLabel.frame = CGRect(x: 4, y: 121/*cell.img.frame.maxY-24*/, width: 60, height: 20)
                typeImg.frame = CGRect(x: sizeLabel.frame.maxX+48, y: 111 /*cell.img.frame.maxY-34*/, width: 30, height: 30)
                statusImg.image = nil
                typeImg.image = UIImage(named: "Img")
                sizeLabel.text = V3ChatHelper.convertKBIntoUnit(diskSpace: m.size)
            }
            else{
                //senders end
                blurView.isHidden = true
               // cell.addReSndButton(show: true)
                textBubleView.frame = CGRect(x: frame.width - 161, y: 5, width: 145, height: 145)
               
                
                let str = m.fileName
               // print(msg.localPath,msg.serverPath)// agar local path nahi hai tho serve path check karo aur download karo
                if m.localPath != ""{
                    let url = URL(fileURLWithPath: m.localPath)
                    let path = url.pathComponents.contains("Receive") ? Medias.ATTAC_Chat_ReceiveIMAGE : Medias.ATTAC_Chat_SentIMAGE
                    img.image = UIImage(contentsOfFile: V3ChatHelper.showImageTaskProfile(name:str, atPath: path).path)?.scaleToSize(aSize: CGSize(width: 145, height: 145))
                    
                }else{
                    // yaha n per server path se dhikhao baad mai download to local ka logic pata karna hai
                    img.sd_setImage(with: URL(string: m.serverPath), placeholderImage: UIImage(named: "radioOn"))
                }
              
                Downloadbtn.isHidden = true
                showbtn.isHidden = false
//                cell.showbtn.myRow = indexpath.item
//                cell.showbtn.mySection = indexpath.section
//                cell.showbtn.addTarget(self, action: #selector(pressShow(_:)), for: .touchUpInside)
                timeLabel.text =  "\(m.date.toStringTime())"
                timeLabel.textAlignment = .right
                                     timeLabel.frame = CGRect(x: frame.width - 100, y: textBubleView.frame.maxY, width: 70, height: 16)
                                     
                                     if m.read == true && m.sent == true{
                                          statusImg.image = UIImage(named: "delivered")
                                        img.loadingIndicator(false)
                                     }else if m.delivered == true{
                                          statusImg.image = UIImage(named: "delivered")
                                        img.loadingIndicator(false)
                                     }else if m.sent == true{
                                          statusImg.image = UIImage(named: "sent")
                                        img.loadingIndicator(false)
                                     }else{
                                        if !m.resend && !m.sent && m.sendTos.count > 1{
                                            img.loadingIndicator(true)}
                                         statusImg.image = UIImage(named: "pending")
                                     }
                                    
                statusImg.frame = CGRect(x: frame.width - 20, y: textBubleView.frame.maxY + 1, width: 18, height: 18)
                sizeLabel.frame = CGRect(x: 4, y: 121/*cell.img.frame.maxY-24*/, width: 60, height: 20)
                
                typeImg.frame = CGRect(x: sizeLabel.frame.maxX+48, y: 111/*cell.img.frame.maxY-34*/, width: 30, height: 30)
                typeImg.image = UIImage(named: "Img")
                nameLabel.text = ""
                if m.size == 0{
                    sizeLabel.isHidden = true
                    //sizeOfFile(msg.fileName, path: "WorkOnDoc/media/Doc/Sent")
                }else{
                    sizeLabel.isHidden = false
                    sizeLabel.text = V3ChatHelper.convertKBIntoUnit(diskSpace: m.size)//msg.size == 0 ? "" : convertKBIntoUnit(diskSpace: msg.size)
                }
                
                
                
                if m.resend{
                    img.loadingIndicator(false)
                    //cell.addReSndButton(show: true)
                    resendBtn.isHidden = false
                    // add action here to resend btn
//                    cell.resendBtn.myRow = indexpath.item
//                    resendBtn.mySection = indexpath.section
//                    resendBtn.addTarget(self, action: #selector(reSendMSG(_:)), for: .touchUpInside)
                }else{
                    resendBtn.isHidden = true
                }
            }
      
            
            
            ////--------------/////
        }
    }
    
    let textBubleView:UIView = {
       let v = UIView()
        //v.backgroundColor = .red//UIColor(white: 0.95, alpha: 1)
        v.layer.cornerRadius = 15
        v.layer.masksToBounds = true
        v.backgroundColor = .white
        return v
    }()
    
    let img:UIImageView = {
        let imagev = UIImageView()
        //imagev.layer.cornerRadius = 8
       // imagev.layer.masksToBounds = true
        imagev.image = #imageLiteral(resourceName: "imgAttach")
        imagev.contentMode = .scaleToFill
        return imagev
    }()
    let blurView:UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .light)
       // let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
          let v = UIVisualEffectView(effect: blurEffect)
        v.alpha = 0.95
           return v
       }()
    
    let Downloadbtn:MyButton = {
        let button = MyButton(frame: .zero)
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "Download"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    let resendBtn:MyButton = {
        let button = MyButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "memories"), for: .normal)
        button.layer.cornerRadius = 25
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    let playbtn:MyButton = {
        let button = MyButton(frame: .zero)
        button.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
        button.layer.cornerRadius = 25
        button.setImage(UIImage(named: "playVideo"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    let showbtn:MyButton = {
        let button = MyButton(frame: .zero)
        button.backgroundColor = .clear
        
       // button.setImage(#imageLiteral(resourceName: "Del"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    // to show image type of message
    let typeImg:UIImageView = {
        let imagev = UIImageView()
        imagev.backgroundColor = UIColor(white: 0.0, alpha: 0.2)//Constants.Design.Color.Primary.v3superbackgroundCol//UIColor(red: 0.849, green: 0.949, blue: 0.975, alpha: 1.0)//Constants.Design.Color.Primary.v3Lightblue//yahan
        imagev.image = #imageLiteral(resourceName: "Vdo")
        imagev.contentMode = .center
        imagev.layer.cornerRadius = 15
        imagev.clipsToBounds = true
        return imagev
    }()
    let sizeLabel:UILabel = {
           let label = UILabel()
           label.font = UIFont(name: "Helvetica", size: 11)
           label.textColor = .black
           //label.text = "48 kb"
           label.sizeToFit()
           label.backgroundColor = .white
           label.textAlignment = .center
           label.layer.cornerRadius = 10
           label.clipsToBounds = true
           return label
       }()
    
    
    let statusImg:UIImageView = {
        let imagev = UIImageView()
      
        return imagev
    }()
    
    let timeLabel:UILabel = {
                  let label = UILabel()
                  label.font = UIFont(name: "Helvetica", size: 10)
                  label.textColor =  .systemGray
                 
                  label.text = ""
                  label.sizeToFit()
                  return label
          }()
    
    let nameLabel:UILabel = {
           let label = UILabel()
           label.font = UIFont(name: "Helvetica", size: 11)
           label.textColor = .gray
           //label.text = "chandra mohan sahu"
           label.sizeToFit()
           return label
       }()
//    override func prepareForReuse() {
//           super.prepareForReuse()
//        //img.image = nil
//
//      }
    
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        addSubview(textBubleView)
        
        textBubleView.addSubview(img)
        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: img)
        textBubleView.addConstraintVisualWith(formate: "V:|[v0]|", views: img)
        textBubleView.addSubview(blurView)
        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: blurView)
        textBubleView.addConstraintVisualWith(formate: "V:|[v0]|", views: blurView)
        
        textBubleView.addSubview(showbtn)
//        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: showbtn)
//        textBubleView.addConstraintVisualWith(formate: "V:|[v0]|", views: showbtn)
        showbtn.centerXAnchor.constraint(equalTo: textBubleView.centerXAnchor).isActive = true
        showbtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor).isActive = true
        showbtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        showbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        
        
        textBubleView.addSubview(playbtn)
        // change contraint cause frame of playbtn obstract the cell interection
        //textBubleView.addConstraintVisualWith(formate: "H:|[v0(20)]|", views: playbtn)
       // textBubleView.addConstraintVisualWith(formate: "V:|[v0(20)]|", views: playbtn)
        playbtn.centerXAnchor.constraint(equalTo: textBubleView.centerXAnchor).isActive = true
        playbtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor).isActive = true
        playbtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        playbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        textBubleView.addSubview(Downloadbtn)
        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: Downloadbtn)
        textBubleView.addConstraintVisualWith(formate: "V:|[v0]|", views: Downloadbtn)
        addSubview(statusImg)
        addSubview(nameLabel)
        addSubview(timeLabel)
       

        addConstraintVisualWith(formate: "H:|-12-[v0]", views: nameLabel)
        addConstraintVisualWith(formate: "V:|-0-[v0]", views: nameLabel)
        
        
        textBubleView.addSubview(sizeLabel)
        
        textBubleView.addSubview(typeImg)
        addSubview(resendBtn)
        addConstraintVisualWith(formate: "H:[v0(50)]-165-|", views: resendBtn)
        addConstraintVisualWith(formate: "V:|-50-[v0(50)]", views: resendBtn)

        //addSubview(img)
        /*
        let imageView = UIImageView(image: UIImage(named: "example"))
        imageView.frame = view.bounds
        imageView.contentMode = .scaleToFill
        view.addSubview(imageView)

        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = imageView.bounds
        view.addSubview(blurredEffectView)
        */
    }
//    func addReSndButton(show:Bool){
//        if show{
//        addSubview(resendBtn)
//        addConstraintVisualWith(formate: "H:[v0(50)]-[v1]-16-|", views: resendBtn,textBubleView)
//            addConstraintVisualWith(formate: "V:|-50-[v0(50)]", views: resendBtn)
//            //resendBtn.isHidden = false
//        }else{
//            resendBtn.removeFromSuperview()
//            addConstraintVisualWith(formate: "H:|-16-[v0(145)]|", views: textBubleView)
//                addConstraintVisualWith(formate: "V:|-14-[v0(145)]", views: textBubleView)
//            //resendBtn.isHidden = true
//        }
//        //layoutSubviews()
//    }
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        layoutSubviews()
////        resendBtn.removeFromSuperview()
////        textBubleView.frame = .zero
//    }
}

class ImageQouteChatCell: BaseClass {
    let textBubleView:UIView = {
       let v = UIView()
        //v.backgroundColor = .red//UIColor(white: 0.95, alpha: 1)
        v.layer.cornerRadius = 15
        v.layer.masksToBounds = true
        v.backgroundColor = .white
        return v
    }()
    
    let img:UIImageView = {
        let imagev = UIImageView()
        //imagev.layer.cornerRadius = 8
       // imagev.layer.masksToBounds = true
        imagev.image = #imageLiteral(resourceName: "imgAttach")
        imagev.contentMode = .scaleToFill
        return imagev
    }()
    let blurView:UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .light)
       // let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
          let v = UIVisualEffectView(effect: blurEffect)
        v.alpha = 0.95
           return v
       }()
    
    let Downloadbtn:MyButton = {
        let button = MyButton(frame: .zero)
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "Download"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let playbtn:MyButton = {
        let button = MyButton(frame: .zero)
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "playVideo"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    let showbtn:MyButton = {
        let button = MyButton(frame: .zero)
        button.backgroundColor = .clear
        
       // button.setImage(#imageLiteral(resourceName: "Del"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    // to show image type of message
    let typeImg:UIImageView = {
        let imagev = UIImageView()
        imagev.backgroundColor = .white
        imagev.image = #imageLiteral(resourceName: "Vdo")
        imagev.contentMode = .center
        imagev.layer.cornerRadius = 15
        imagev.clipsToBounds = true
        return imagev
    }()
    let sizeLabel:UILabel = {
           let label = UILabel()
           label.font = UIFont(name: "Helvetica", size: 11)
           label.textColor = .black
           //label.text = "48 kb"
           label.sizeToFit()
           label.backgroundColor = .white
           label.textAlignment = .center
           label.layer.cornerRadius = 10
           label.clipsToBounds = true
           return label
       }()
    
    
    let statusImg:UIImageView = {
        let imagev = UIImageView()
      
        return imagev
    }()
    
    let timeLabel:UILabel = {
                  let label = UILabel()
                  label.font = UIFont(name: "Helvetica", size: 10)
                  label.textColor =  .systemGray
                 
                  label.text = ""
                  label.sizeToFit()
                  return label
          }()
    
    let nameLabel:UILabel = {
           let label = UILabel()
           label.font = UIFont(name: "Helvetica", size: 11)
           label.textColor = .gray
           //label.text = "chandra mohan sahu"
           label.sizeToFit()
           return label
       }()
    
   // component for quote design
    let quoteview:UIView = {
        let v = UIView()
        v.layer.cornerRadius = 4
        v.backgroundColor = .secondarySystemBackground
        v.layer.masksToBounds = true
        return v
    }()
    
    let lv:UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    let qmLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 12)
        label.textColor = .gray
       // label.backgroundColor = .cyan
        label.numberOfLines = 3
        label.sizeToFit()
        //label.text = "hvsjhcgasd hviuvj hjvuiy uyvu uyvuy dsuyv cluy lwdyubc ucyve wcujwyevbcu ewceuwyvc uwecuwecuewyv cuewcueywc utvewu "
        return label
    }()
    let sentQMLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)//UIFont(name: "Helvetica", size: 12)
        label.textColor = .red
        //label.text = "You"
        label.numberOfLines = 1
      
        return label
    }()
    let qmsgImg:UIImageView = {
        let imagev = UIImageView()
        imagev.backgroundColor = .clear
        //imagev.image = #imageLiteral(resourceName: "imgAttach")
        imagev.contentMode = .scaleAspectFit
        imagev.layer.cornerRadius = 4
        imagev.clipsToBounds = true
        return imagev
    }()
    let resendBtn:MyButton = {
        let button = MyButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "memories"), for: .normal)
        button.layer.cornerRadius = 25
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        addSubview(textBubleView)
        addSubview(quoteview)
        textBubleView.addSubview(img)
        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: img)
        textBubleView.addConstraintVisualWith(formate: "V:|-58-[v0]|", views: img)
        textBubleView.addSubview(blurView)
        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: blurView)
        textBubleView.addConstraintVisualWith(formate: "V:|-58-[v0]|", views: blurView)
        
        textBubleView.addSubview(showbtn)
//        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: showbtn)
//        textBubleView.addConstraintVisualWith(formate: "V:|[v0]|", views: showbtn)
        showbtn.centerXAnchor.constraint(equalTo: textBubleView.centerXAnchor).isActive = true
        showbtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor).isActive = true
        showbtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        showbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        
        
        textBubleView.addSubview(playbtn)
        // change contraint cause frame of playbtn obstract the cell interection
        //textBubleView.addConstraintVisualWith(formate: "H:|[v0(20)]|", views: playbtn)
       // textBubleView.addConstraintVisualWith(formate: "V:|[v0(20)]|", views: playbtn)
        playbtn.centerXAnchor.constraint(equalTo: textBubleView.centerXAnchor).isActive = true
        playbtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor).isActive = true
        playbtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        playbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        textBubleView.addSubview(Downloadbtn)
        textBubleView.addConstraintVisualWith(formate: "H:|[v0]|", views: Downloadbtn)
        textBubleView.addConstraintVisualWith(formate: "V:|-58-[v0]|", views: Downloadbtn)
        addSubview(statusImg)
        addSubview(nameLabel)
        addSubview(timeLabel)

        addConstraintVisualWith(formate: "H:|-12-[v0]", views: nameLabel)
        addConstraintVisualWith(formate: "V:|-0-[v0]", views: nameLabel)
        
        
        
        textBubleView.addSubview(sizeLabel)
        
        textBubleView.addSubview(typeImg)
        
        quoteview.addSubview(qmLabel)
                quoteview.addSubview(lv)
                quoteview.addSubview(sentQMLabel)
                quoteview.addSubview(qmsgImg)
                quoteview.addConstraintVisualWith(formate: "H:|[v0(3)]-2-[v1]-2-|", views: lv,qmLabel)
                quoteview.addConstraintVisualWith(formate: "V:|[v0]|", views: lv)
                quoteview.addConstraintVisualWith(formate: "V:|-2-[v0]-2-[v1]", views: sentQMLabel,qmLabel)
                quoteview.addConstraintVisualWith(formate: "H:|-5-[v0]", views: sentQMLabel)
                quoteview.addConstraintVisualWith(formate: "V:|-4-[v0(40)]", views: qmsgImg)
                quoteview.addConstraintVisualWith(formate: "H:[v0(40)]-4-|", views: qmsgImg)
        
        
        addSubview(resendBtn)
        addConstraintVisualWith(formate: "H:[v0(50)]-165-|", views: resendBtn)
        addConstraintVisualWith(formate: "V:|-50-[v0(50)]", views: resendBtn)
        //addSubview(img)
        /*
        let imageView = UIImageView(image: UIImage(named: "example"))
        imageView.frame = view.bounds
        imageView.contentMode = .scaleToFill
        view.addSubview(imageView)

        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = imageView.bounds
        view.addSubview(blurredEffectView)
        */
    }
}

class DefaultChatCell: BaseClass {
    
    
   
    
    let nameLabel:UILabel = {
        let label = PaddingLabel(withInsets: 8, 8, 16, 16)
           label.font = UIFont(name: "Helvetica", size: 12)
           label.textColor = .gray
        label.textAlignment = .center
           label.layer.cornerRadius = 12
           label.backgroundColor = HElper.hexStringToUIColor(hex: "#dcf8c6")
           label.clipsToBounds = true
           label.translatesAutoresizingMaskIntoConstraints = false
           label.sizeToFit()
           return label
       }()
   
    
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        addSubview(nameLabel)
        nameLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor,constant: 0).isActive = true
//        addConstraintVisualWith(formate: "H:|[v0(225)]|", views: nameLabel)
//        addConstraintVisualWith(formate: "V:|[v0(30)]|", views: nameLabel)
        
       
      //  nameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    //   nameLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//        nameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
//              nameLabel.widthAnchor.constraint(equalToConstant: contentView.frame.width).isActive = true
       
    }
}
/*protocol audioChatcellDelegate:class {
    func playFromcell()
    func pauseFromCell()
    func stopFromCell()
}*/

class AudioChatCell: BaseClass {
   // weak var delegate: audioChatcellDelegate?
    let textBubleView:UIView = {
          let v = UIView()
           //v.backgroundColor = .red//UIColor(white: 0.95, alpha: 1)
           v.layer.cornerRadius = 15
           v.layer.masksToBounds = true
           v.backgroundColor = .white
           return v
       }()
       
       let img:UIImageView = {
           let imagev = UIImageView()
           //imagev.layer.cornerRadius = 8
          // imagev.layer.masksToBounds = true
           imagev.image = #imageLiteral(resourceName: "soundWave")
           imagev.contentMode = .scaleToFill
           return imagev
       }()
    
    let playbtn:MyButton = {
           let button = MyButton(frame: .zero)
           button.backgroundColor = .clear
           button.setImage(UIImage(named: "playVideo"), for: .normal)
           button.translatesAutoresizingMaskIntoConstraints = false
           //button.addTarget(self, action: #selector(playAudio(_:)), for: .touchUpInside)
           return button
       }()
   
    let Downloadbtn:MyButton = {
           let button = MyButton(frame: .zero)
        button.backgroundColor = .lightGray
           button.setImage(UIImage(named: "Download"), for: .normal)
           button.translatesAutoresizingMaskIntoConstraints = false
           return button
       }()
    
    
    let nameLabel:UILabel = {
        let label = PaddingLabel(withInsets: 8, 8, 16, 16)
           label.font = UIFont(name: "Helvetica", size: 12)
           label.textColor = .gray
           label.layer.cornerRadius = 12
           //label.backgroundColor = HElper.hexStringToUIColor(hex: "#dcf8c6")
           label.clipsToBounds = true
           //label.translatesAutoresizingMaskIntoConstraints = false
           label.sizeToFit()
           return label
       }()
   
           let statusImg:UIImageView = {
                  let imagev = UIImageView()
                  return imagev
              }()
              let timeLabel:UILabel = {
                            let label = UILabel()
                            label.font = UIFont(name: "Helvetica", size: 10)
                            label.textColor =  .systemGray
                           
                            label.text = ""
                            label.sizeToFit()
                            return label
                    }()
              
    // component for quote design
     let quoteview:UIView = {
         let v = UIView()
         v.layer.cornerRadius = 4
         v.backgroundColor = .secondarySystemBackground
         v.layer.masksToBounds = true
         return v
     }()
     
     let lv:UIView = {
         let v = UIView()
         v.backgroundColor = .red
         return v
     }()
     let qmLabel:UILabel = {
         let label = UILabel()
         label.font = UIFont(name: "Helvetica", size: 12)
         label.textColor = .gray
        // label.backgroundColor = .cyan
         label.numberOfLines = 3
         label.sizeToFit()
         //label.text = "hvsjhcgasd hviuvj hjvuiy uyvu uyvuy dsuyv cluy lwdyubc ucyve wcujwyevbcu ewceuwyvc uwecuwecuewyv cuewcueywc utvewu "
         return label
     }()
     let sentQMLabel:UILabel = {
         let label = UILabel()
         label.font = UIFont.boldSystemFont(ofSize: 12)//UIFont(name: "Helvetica", size: 12)
         label.textColor = .red
         //label.text = "You"
         label.numberOfLines = 1
       
         return label
     }()
     let qmsgImg:UIImageView = {
         let imagev = UIImageView()
         imagev.backgroundColor = .clear
         //imagev.image = #imageLiteral(resourceName: "imgAttach")
         imagev.contentMode = .scaleAspectFit
         imagev.layer.cornerRadius = 4
         imagev.clipsToBounds = true
         return imagev
     }()
    let resendBtn:MyButton = {
        let button = MyButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "memories"), for: .normal)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
   
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        addSubview(nameLabel)
      
       addSubview(textBubleView)
        addSubview(quoteview)

       textBubleView.addSubview(img)
       addSubview(statusImg)
       addSubview(timeLabel)
       textBubleView.addConstraintVisualWith(formate: "H:|-50-[v0]|", views: img)
       textBubleView.addConstraintVisualWith(formate: "V:|-4-[v0]-4-|", views: img)
       
       
  
       
       textBubleView.addSubview(playbtn)
              textBubleView.addConstraintVisualWith(formate: "H:|-1-[v0(50)]|", views: playbtn)
              textBubleView.addConstraintVisualWith(formate: "V:|[v0(40)]|", views: playbtn)
       
       textBubleView.addSubview(Downloadbtn)
       textBubleView.addConstraintVisualWith(formate: "H:|-1-[v0(50)]|", views: Downloadbtn)
       textBubleView.addConstraintVisualWith(formate: "V:|[v0(50)]|", views: Downloadbtn)
        addConstraintVisualWith(formate: "H:|-2-[v0]", views: nameLabel)
        addConstraintVisualWith(formate: "V:|-0-[v0]", views: nameLabel)
//        addConstraintVisualWith(formate: "H:|[v0]|", views: nameLabel)
//        addConstraintVisualWith(formate: "V:|[v0]|", views: nameLabel)
//        addSubview(playbtn)
//                             addConstraintVisualWith(formate: "H:|[v0]|", views: playbtn)
//                             addConstraintVisualWith(formate: "V:|[v0]|", views: playbtn)
//       //nameLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
//      // nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
////        nameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
////              nameLabel.widthAnchor.constraint(equalToConstant: contentView.frame.width).isActive = true
        quoteview.addSubview(qmLabel)
                quoteview.addSubview(lv)
                quoteview.addSubview(sentQMLabel)
                quoteview.addSubview(qmsgImg)
                quoteview.addConstraintVisualWith(formate: "H:|[v0(3)]-2-[v1]-2-|", views: lv,qmLabel)
                quoteview.addConstraintVisualWith(formate: "V:|[v0]|", views: lv)
                quoteview.addConstraintVisualWith(formate: "V:|-2-[v0]-2-[v1]", views: sentQMLabel,qmLabel)
                quoteview.addConstraintVisualWith(formate: "H:|-5-[v0]", views: sentQMLabel)
                quoteview.addConstraintVisualWith(formate: "V:|-4-[v0(40)]", views: qmsgImg)
                quoteview.addConstraintVisualWith(formate: "H:[v0(40)]-4-|", views: qmsgImg)
        
        
            addSubview(resendBtn)
        
        
            let horizontalConstraint = resendBtn.trailingAnchor.constraint(equalTo: textBubleView.leadingAnchor)
               let verticalConstraint = resendBtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor)
               let widthConstraint = resendBtn.widthAnchor.constraint(equalToConstant: 30)
               let heightConstraint = resendBtn.heightAnchor.constraint(equalToConstant: 30)
               addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    }
//    @objc func playAudio(_ sender: MyButton){
//        delegate?.playFromcell()
//    }
}
class DocChatCell: BaseClass {
    
    let textBubleView:UIView = {
          let v = UIView()
           //v.backgroundColor = .red//UIColor(white: 0.95, alpha: 1)
           v.layer.cornerRadius = 15
           v.layer.masksToBounds = true
           v.backgroundColor = .white
           return v
       }()
       
       let img:UIImageView = {
           let imagev = UIImageView()
           //imagev.layer.cornerRadius = 8
          // imagev.layer.masksToBounds = true
           imagev.image = #imageLiteral(resourceName: "pdf")
           imagev.contentMode = .scaleToFill
           return imagev
       }()
    
   
   
    let Downloadbtn:MyButton = {
           let button = MyButton(frame: .zero)
        button.backgroundColor = .lightGray
           button.setImage(UIImage(named: "Download"), for: .normal)
           button.translatesAutoresizingMaskIntoConstraints = false
           return button
       }()
    
    
    let nameLabel:UILabel = {
        let label = PaddingLabel(withInsets: 0, 0, 5, 0)
           label.font = UIFont(name: "Helvetica", size: 12)
           label.textColor = .gray
          // label.layer.cornerRadius = 12
           //label.backgroundColor = HElper.hexStringToUIColor(hex: "#dcf8c6")
           label.clipsToBounds = true
        label.numberOfLines = 2
           //label.translatesAutoresizingMaskIntoConstraints = false
           label.sizeToFit()
        label.text = ""
           return label
       }()
    let sizeLbl:UILabel = {
           let label = PaddingLabel(withInsets: 0, 0, 0, 0)
              label.font = UIFont(name: "Helvetica", size: 10)
              label.textColor = .gray
              //label.layer.cornerRadius = 10
              //label.backgroundColor = HElper.hexStringToUIColor(hex: "#dcf8c6")
              label.clipsToBounds = true
              //label.translatesAutoresizingMaskIntoConstraints = false
              label.sizeToFit()
        label.text = ""
              return label
          }()
    let playbtn:MyButton = {
              let button = MyButton(frame: .zero)
              button.backgroundColor = .clear
              //button.setImage(UIImage(named: "playVideo"), for: .normal)
              button.translatesAutoresizingMaskIntoConstraints = false
              return button
          }()
    let statusImg:UIImageView = {
                     let imagev = UIImageView()
                     return imagev
                 }()
                 let timeLabel:UILabel = {
                               let label = UILabel()
                               label.font = UIFont(name: "Helvetica", size: 10)
                               label.textColor =  .systemGray
                              
                               label.text = ""
                               label.sizeToFit()
                               return label
                       }()
                 let senderLabel:UILabel = {
                  let label = PaddingLabel(withInsets: 8, 8, 16, 16)
                     label.font = UIFont(name: "Helvetica", size: 12)
                     label.textColor = .gray
                     label.layer.cornerRadius = 12
                     //label.backgroundColor = HElper.hexStringToUIColor(hex: "#dcf8c6")
                     label.clipsToBounds = true
                     //label.translatesAutoresizingMaskIntoConstraints = false
                     label.sizeToFit()
                     return label
                 }()
    // component for quote design
     let quoteview:UIView = {
         let v = UIView()
         v.layer.cornerRadius = 4
         v.backgroundColor = .secondarySystemBackground
         v.layer.masksToBounds = true
         return v
     }()
     
     let lv:UIView = {
         let v = UIView()
         v.backgroundColor = .red
         return v
     }()
     let qmLabel:UILabel = {
         let label = UILabel()
         label.font = UIFont(name: "Helvetica", size: 12)
         label.textColor = .gray
        // label.backgroundColor = .cyan
         label.numberOfLines = 3
         label.sizeToFit()
         //label.text = "hvsjhcgasd hviuvj hjvuiy uyvu uyvuy dsuyv cluy lwdyubc ucyve wcujwyevbcu ewceuwyvc uwecuwecuewyv cuewcueywc utvewu "
         return label
     }()
     let sentQMLabel:UILabel = {
         let label = UILabel()
         label.font = UIFont.boldSystemFont(ofSize: 12)//UIFont(name: "Helvetica", size: 12)
         label.textColor = .red
         //label.text = "You"
         label.numberOfLines = 1
       
         return label
     }()
     let qmsgImg:UIImageView = {
         let imagev = UIImageView()
         imagev.backgroundColor = .clear
         //imagev.image = #imageLiteral(resourceName: "imgAttach")
         imagev.contentMode = .scaleAspectFit
         imagev.layer.cornerRadius = 4
         imagev.clipsToBounds = true
         return imagev
     }()
    let resendBtn:MyButton = {
        let button = MyButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "memories"), for: .normal)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
   
    override func setupSubViews() {
    super.setupSubViews()
        backgroundColor = .clear
        
        addSubview(nameLabel)
        addSubview(statusImg)
            addSubview(timeLabel)
       addSubview(textBubleView)
        addSubview(quoteview)
        addSubview(senderLabel)
       textBubleView.addSubview(img)
       textBubleView.addConstraintVisualWith(formate: "H:|-8-[v0(30)]|", views: img)
       textBubleView.addConstraintVisualWith(formate: "V:|-4-[v0]-4-|", views: img)
       
       textBubleView.addSubview(nameLabel)
       textBubleView.addConstraintVisualWith(formate: "H:|-46-[v0]|", views: nameLabel)
      // textBubleView.addConstraintVisualWith(formate: "V:|-6-[v0]|", views: nameLabel)
       
        textBubleView.addSubview(sizeLbl)
        textBubleView.addConstraintVisualWith(formate: "H:|-46-[v0]|", views: sizeLbl)
        textBubleView.addConstraintVisualWith(formate: "V:|-6-[v0]-4-[v1]", views: nameLabel,sizeLbl)
        
  
        //
      
       
       textBubleView.addSubview(Downloadbtn)
       textBubleView.addConstraintVisualWith(formate: "H:|-1-[v0(50)]|", views: Downloadbtn)
       textBubleView.addConstraintVisualWith(formate: "V:|[v0(50)]|", views: Downloadbtn)
        
        textBubleView.addSubview(playbtn)
              textBubleView.addConstraintVisualWith(formate: "H:|-1-[v0(60)]|", views: playbtn)
              textBubleView.addConstraintVisualWith(formate: "V:|[v0(50)]|", views: playbtn)
        addConstraintVisualWith(formate: "H:|-2-[v0]", views: senderLabel)
        addConstraintVisualWith(formate: "V:|-0-[v0]", views: senderLabel)
//        addConstraintVisualWith(formate: "H:|[v0]|", views: nameLabel)
//        addConstraintVisualWith(formate: "V:|[v0]|", views: nameLabel)
//        addSubview(playbtn)
//                             addConstraintVisualWith(formate: "H:|[v0]|", views: playbtn)
//                             addConstraintVisualWith(formate: "V:|[v0]|", views: playbtn)
//       //nameLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
//      // nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
////        nameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
////              nameLabel.widthAnchor.constraint(equalToConstant: contentView.frame.width).isActive = true
        
        quoteview.addSubview(qmLabel)
               quoteview.addSubview(lv)
               quoteview.addSubview(sentQMLabel)
               quoteview.addSubview(qmsgImg)
               quoteview.addConstraintVisualWith(formate: "H:|[v0(3)]-2-[v1]-2-|", views: lv,qmLabel)
               quoteview.addConstraintVisualWith(formate: "V:|[v0]|", views: lv)
               quoteview.addConstraintVisualWith(formate: "V:|-2-[v0]-2-[v1]", views: sentQMLabel,qmLabel)
               quoteview.addConstraintVisualWith(formate: "H:|-5-[v0]", views: sentQMLabel)
               quoteview.addConstraintVisualWith(formate: "V:|-4-[v0(40)]", views: qmsgImg)
               quoteview.addConstraintVisualWith(formate: "H:[v0(40)]-4-|", views: qmsgImg)
            addSubview(resendBtn)
        
        
            let horizontalConstraint = resendBtn.trailingAnchor.constraint(equalTo: textBubleView.leadingAnchor)
               let verticalConstraint = resendBtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor)
               let widthConstraint = resendBtn.widthAnchor.constraint(equalToConstant: 30)
               let heightConstraint = resendBtn.heightAnchor.constraint(equalToConstant: 30)
               addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
       
    }
}

class ConversationQuoteBackCell:BaseClass{
    
    let messageTextView:UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.text = ""
        tv.backgroundColor = .clear
        tv.isEditable = false
        return tv
    }()
    let textBubleView:UIView = {
       let v = UIView()
        //v.backgroundColor = .red//UIColor(white: 0.95, alpha: 1)
        //v.layer.cornerRadius = 15
        //v.layer.masksToBounds = true
        return v
    }()
    let quoteview:UIView = {
        let v = UIView()
        v.layer.cornerRadius = 4
        v.backgroundColor = .secondarySystemBackground
        v.layer.masksToBounds = true
        return v
    }()
    
    let bubble:UIImageView = {
        let imagev = UIImageView()
        imagev.image = ConversationCell.grayBuble//UIImage(named: "leftB")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysOriginal)
        return imagev
    }()
    static let grayBuble = UIImage(named: "leftB")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    static let blueBuble = UIImage(named: "RBuble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    let statusImg:UIImageView = {
        let imagev = UIImageView()
        return imagev
    }()
    let timeLabel:UILabel = {
               let label = UILabel()
               label.font = UIFont(name: "Helvetica", size: 10)
               label.textColor =  .systemGray
              
               label.text = "cjhsgck"
               label.sizeToFit()
               return label
       }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 11)
        label.textColor = .gray
        //label.text = "chandra mohan sahu"
        label.sizeToFit()
        return label
    }()
    
   // component for quote design
    let lv:UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    let qmLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 12)
        label.textColor = .gray
        //label.backgroundColor = .cyan
        label.numberOfLines = 3
        label.sizeToFit()
        //label.text = "hvsjhcgasd hviuvj hjvuiy uyvu uyvuy dsuyv cluy lwdyubc ucyve wcujwyevbcu ewceuwyvc uwecuwecuewyv cuewcueywc utvewu "
        return label
    }()
    let sentQMLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)//UIFont(name: "Helvetica", size: 12)
        label.textColor = .red
        label.text = "You"
        label.numberOfLines = 1
      
        return label
    }()
    let qmsgImg:UIImageView = {
        let imagev = UIImageView()
        imagev.backgroundColor = .clear
        //imagev.image = #imageLiteral(resourceName: "imgAttach")
        imagev.contentMode = .scaleAspectFit
        imagev.layer.cornerRadius = 4
        imagev.clipsToBounds = true
        return imagev
    }()
    let resendBtn:MyButton = {
        let button = MyButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "memories"), for: .normal)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
//    addSubview(resendBtn)
//
//
//    let horizontalConstraint = resendBtn.trailingAnchor.constraint(equalTo: textBubleView.leadingAnchor)
//       let verticalConstraint = resendBtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor)
//       let widthConstraint = resendBtn.widthAnchor.constraint(equalToConstant: 30)
//       let heightConstraint = resendBtn.heightAnchor.constraint(equalToConstant: 30)
//       addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    override func setupSubViews() {
        super.setupSubViews()
        backgroundColor = .clear
        addSubview(textBubleView)
        addSubview(messageTextView)
        addSubview(quoteview)
        addSubview(nameLabel)
        textBubleView.addSubview(bubble)
       
        addSubview(timeLabel)
        addSubview(statusImg)
        addConstraintVisualWith(formate: "H:|[v0]|", views: bubble)
        addConstraintVisualWith(formate: "V:|[v0]|", views: bubble)
        
        addConstraintVisualWith(formate: "H:|-12-[v0]", views: nameLabel)
        addConstraintVisualWith(formate: "V:|-4-[v0]", views: nameLabel)
        
//        textBubleView.addContraintWithFormat(format: "H:|-2-[v0]-2-|", views: quoteview)
//        textBubleView.addContraintWithFormat(format: "V:|[v0]", views: quoteview)
       
        quoteview.addSubview(qmLabel)
        quoteview.addSubview(lv)
        quoteview.addSubview(sentQMLabel)
        quoteview.addSubview(qmsgImg)
        quoteview.addConstraintVisualWith(formate: "H:|[v0(3)]-2-[v1]-2-|", views: lv,qmLabel)
        quoteview.addConstraintVisualWith(formate: "V:|[v0]|", views: lv)
        quoteview.addConstraintVisualWith(formate: "V:|-2-[v0]-2-[v1]", views: sentQMLabel,qmLabel)
        quoteview.addConstraintVisualWith(formate: "H:|-5-[v0]", views: sentQMLabel)
        quoteview.addConstraintVisualWith(formate: "V:|-4-[v0(40)]", views: qmsgImg)
        quoteview.addConstraintVisualWith(formate: "H:[v0(40)]-4-|", views: qmsgImg)
        
        addSubview(resendBtn)
        
        
        let horizontalConstraint = resendBtn.trailingAnchor.constraint(equalTo: textBubleView.leadingAnchor)
           let verticalConstraint = resendBtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor)
           let widthConstraint = resendBtn.widthAnchor.constraint(equalToConstant: 30)
           let heightConstraint = resendBtn.heightAnchor.constraint(equalToConstant: 30)
           addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
//        pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
//           pan.delegate = self
//           self.addGestureRecognizer(pan)
       
    }
   /* @objc func onPan(_ pan: UIPanGestureRecognizer) {
        if pan.state == UIGestureRecognizer.State.began {

        } else if pan.state == UIGestureRecognizer.State.changed {
          self.setNeedsLayout()
        } else {
          if abs(pan.velocity(in: self).x) > 500 {
            let collectionView: UICollectionView = self.superview as! UICollectionView
            let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
            collectionView.delegate?.collectionView!(collectionView, performAction: #selector(onPan(_:)), forItemAt: indexPath, withSender: nil)
          } else {
            UIView.animate(withDuration: 0.2, animations: {
              self.setNeedsLayout()
              self.layoutIfNeeded()
            })
          }
        }
      }*/
}

//ConversationQuoteBackCell
class ReminiderCell:BaseClass{
    
    let messageTextView:UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.text = ""
        tv.backgroundColor = .clear
        tv.isEditable = false
        return tv
    }()
    let textBubleView:UIView = {
       let v = UIView()
        //v.backgroundColor = .red//UIColor(white: 0.95, alpha: 1)
        //v.layer.cornerRadius = 15
        //v.layer.masksToBounds = true
        return v
    }()
    let quoteview:UIView = {
        let v = UIView()
        v.layer.cornerRadius = 4
        v.backgroundColor = .secondarySystemBackground
        v.layer.masksToBounds = true
        return v
    }()
    
    let bubble:UIImageView = {
        let imagev = UIImageView()
        imagev.image = ConversationCell.grayBuble//UIImage(named: "leftB")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysOriginal)
        return imagev
    }()
    static let grayBuble = UIImage(named: "leftB")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    static let blueBuble = UIImage(named: "RBuble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    let statusImg:UIImageView = {
        let imagev = UIImageView()
        return imagev
    }()
    let timeLabel:UILabel = {
               let label = UILabel()
               label.font = UIFont(name: "Helvetica", size: 10)
               label.textColor =  .systemGray
              
               label.text = "cjhsgck"
               label.sizeToFit()
               return label
       }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 11)
        label.textColor = .gray
        //label.text = "chandra mohan sahu"
        label.sizeToFit()
        return label
    }()
    
   // component for quote design
    let lv:UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    let qmLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 12)
        label.textColor = .gray
        //label.backgroundColor = .cyan
        label.numberOfLines = 3
        label.sizeToFit()
        //label.text = "hvsjhcgasd hviuvj hjvuiy uyvu uyvuy dsuyv cluy lwdyubc ucyve wcujwyevbcu ewceuwyvc uwecuwecuewyv cuewcueywc utvewu "
        return label
    }()
    let sentQMLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)//UIFont(name: "Helvetica", size: 12)
        label.textColor = .red
        label.text = "You"
        label.numberOfLines = 1
      
        return label
    }()
    let qmsgImg:UIImageView = {
        let imagev = UIImageView()
        imagev.backgroundColor = .clear
        //imagev.image = #imageLiteral(resourceName: "imgAttach")
        imagev.contentMode = .scaleAspectFit
        imagev.layer.cornerRadius = 4
        imagev.clipsToBounds = true
        return imagev
    }()
    let resendBtn:MyButton = {
        let button = MyButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "memories"), for: .normal)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
//    addSubview(resendBtn)
//
//
//    let horizontalConstraint = resendBtn.trailingAnchor.constraint(equalTo: textBubleView.leadingAnchor)
//       let verticalConstraint = resendBtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor)
//       let widthConstraint = resendBtn.widthAnchor.constraint(equalToConstant: 30)
//       let heightConstraint = resendBtn.heightAnchor.constraint(equalToConstant: 30)
//       addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    override func setupSubViews() {
        super.setupSubViews()
        backgroundColor = .clear
        addSubview(textBubleView)
        addSubview(messageTextView)
        addSubview(quoteview)
        addSubview(nameLabel)
        textBubleView.addSubview(bubble)
       
        addSubview(timeLabel)
        addSubview(statusImg)
        addConstraintVisualWith(formate: "H:|[v0]|", views: bubble)
        addConstraintVisualWith(formate: "V:|[v0]|", views: bubble)
        
        addConstraintVisualWith(formate: "H:|-12-[v0]", views: nameLabel)
        addConstraintVisualWith(formate: "V:|-4-[v0]", views: nameLabel)
        
//        textBubleView.addContraintWithFormat(format: "H:|-2-[v0]-2-|", views: quoteview)
//        textBubleView.addContraintWithFormat(format: "V:|[v0]", views: quoteview)
       
        quoteview.addSubview(qmLabel)
        quoteview.addSubview(lv)
        quoteview.addSubview(sentQMLabel)
        quoteview.addSubview(qmsgImg)
        quoteview.addConstraintVisualWith(formate: "H:|[v0(3)]-2-[v1]-2-|", views: lv,qmLabel)
        quoteview.addConstraintVisualWith(formate: "V:|[v0]|", views: lv)
        quoteview.addConstraintVisualWith(formate: "V:|-2-[v0]-2-[v1]", views: sentQMLabel,qmLabel)
        quoteview.addConstraintVisualWith(formate: "H:|-5-[v0]", views: sentQMLabel)
        quoteview.addConstraintVisualWith(formate: "V:|-4-[v0(40)]", views: qmsgImg)
        quoteview.addConstraintVisualWith(formate: "H:[v0(40)]-4-|", views: qmsgImg)
        
        addSubview(resendBtn)
        
        
        let horizontalConstraint = resendBtn.trailingAnchor.constraint(equalTo: textBubleView.leadingAnchor)
           let verticalConstraint = resendBtn.centerYAnchor.constraint(equalTo: textBubleView.centerYAnchor)
           let widthConstraint = resendBtn.widthAnchor.constraint(equalToConstant: 30)
           let heightConstraint = resendBtn.heightAnchor.constraint(equalToConstant: 30)
           addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
//        pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
//           pan.delegate = self
//           self.addGestureRecognizer(pan)
       
    }
   /* @objc func onPan(_ pan: UIPanGestureRecognizer) {
        if pan.state == UIGestureRecognizer.State.began {

        } else if pan.state == UIGestureRecognizer.State.changed {
          self.setNeedsLayout()
        } else {
          if abs(pan.velocity(in: self).x) > 500 {
            let collectionView: UICollectionView = self.superview as! UICollectionView
            let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
            collectionView.delegate?.collectionView!(collectionView, performAction: #selector(onPan(_:)), forItemAt: indexPath, withSender: nil)
          } else {
            UIView.animate(withDuration: 0.2, animations: {
              self.setNeedsLayout()
              self.layoutIfNeeded()
            })
          }
        }
      }*/
}

*/

