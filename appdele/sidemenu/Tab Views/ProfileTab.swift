//
//  ProfileTab.swift
//  appdele
//
//  Created by Danish computer technologies on 25/02/22.
//

import UIKit

class ProfileTabVC:UIViewController{
    let labelOne:UILabel = {
            let label = UILabel()
        //label.backgroundColor = .green//Constants.Design.Color.Primary.v3txtfFontCol
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)//Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "5"
        label.numberOfLines = 1
        label.textAlignment = .center
       // label.sizeToFit()
            return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        //view.backgroundColor = .yellow
        setuptest()
    }
    func  setuptest(){
        view.addSubview(labelOne)
        labelOne.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        labelOne.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismisst))
        labelOne.isUserInteractionEnabled = true
        labelOne.addGestureRecognizer(tap)
    }
    @objc func dismisst(){
        self.navigationController?.popViewController(animated: true)
    }
}
