//
//  HomeTab.swift
//  appdele
//
//  Created by Danish computer technologies on 25/02/22.
//

import UIKit

class HomeTabVC:UICollectionViewController{
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 10 {
            timer?.invalidate()
            startTimer()
        }else{
            timer?.invalidate()
//            incre = 0
        }
    }
    init () {
       // let layout = UICollectionViewFlowLayout()
        //comp layout
        
        super.init(collectionViewLayout: /*layout*/HomeTabVC.createLayout())
    }
    
                static func createLayout()-> UICollectionViewCompositionalLayout{
                    return UICollectionViewCompositionalLayout { sectionNumber, env in
                       // item .fractionalWidth(1.0) .fractionalHeight(1.0)  group .fractionalWidth(1.0) .absolute(200)  .estimated(500)
                        
                        if sectionNumber == 0{
                        
                        let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0)))
                        item.contentInsets.trailing = 16
                        item.contentInsets.leading = 16
                        
                            let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.2)), subitems: [item])
                        
                            group.contentInsets.top = 30
                            let section = NSCollectionLayoutSection(group: group)
                        section.orthogonalScrollingBehavior = .paging // use for scroll else it render within the frame
                            //section.contentInsets.top = 30
                            section.boundarySupplementaryItems = [
                               .init(layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(30)), elementKind:footerPAgeControl, alignment: .bottomLeading)
                            ]
                            return section
                            
                        }
                        else if sectionNumber == 1{
                            let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalHeight(1.0)))
                            item.contentInsets.trailing = 16
                            item.contentInsets.bottom = 16
                            //item.contentInsets.leading = 16
                            
                            let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(100)), subitems: [item])
                            
                                let section = NSCollectionLayoutSection(group: group)
                           section.contentInsets.leading = 16
                            section.contentInsets.top = 8
                           // section.orthogonalScrollingBehavior = .continuous

                            
                            
                                return section
                            
                        }
                        else if sectionNumber == 2{
                            let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1)))
                            item.contentInsets.trailing = 16
                            //item.contentInsets.bottom = 16
                            
                            let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(0.29), heightDimension: .estimated(150)), subitems: [item])
                            
                                
                                let section = NSCollectionLayoutSection(group: group)
                            section.orthogonalScrollingBehavior = .continuous
                            section.contentInsets.leading = 16
                            section.contentInsets.bottom = 16
                            section.boundarySupplementaryItems = [
                                .init(layoutSize: .init(widthDimension: .fractionalWidth(1.0),
                                                        heightDimension: .absolute(50)), elementKind: catgoryld, alignment: .topLeading)
                            ]
                            
                                return section
                        }else {
                            let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0)))
                            item.contentInsets.trailing = 16
                            //item.contentInsets.bottom = 16
                            
                            let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(0.29), heightDimension: .estimated(150)), subitems: [item])
                            
                                
                                let section = NSCollectionLayoutSection(group: group)
                            section.orthogonalScrollingBehavior = .continuous
                            section.contentInsets = .init(top:0, leading:16, bottom:220, trailing:0)
                            section.boundarySupplementaryItems = [
                                .init(layoutSize: .init(widthDimension: .fractionalWidth(1.0),
                                                        heightDimension: .absolute(50)), elementKind: catgoryld, alignment: .topLeading)
                            ]
                                return section
                        }
                    }
                }
                
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        /*
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HEader
       
        return header
        */
         switch kind {
                
         case HomeTabVC.catgoryld:
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HEader
             headerView.label.text = indexPath.section == 2 ? "Categories" : "Newley Registered on ZeeDeeSpeedy"
                
               // headerView.backgroundColor = UIColor.blue
                return headerView
                
         case HomeTabVC.footerPAgeControl:
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerid, for: indexPath) as! Footer
                
               // footerView.backgroundColor = UIColor.green
                return footerView
                
            default:
                
                assert(false, "Unexpected element kind")
            }
         
    }
                
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let cellId = "cellid"
    static let catgoryld = "catgoryld"
    static let footerPAgeControl = "footerPAgeControl"
     let headerId = "headerId"
    let footerid = "pagecontrol"
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
         4
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{return self.imagArr.count }else if section == 1{
            return 2
        }
        return 7
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCell.cellId, for: indexPath) as! BannerCell
            cell.bannerImg.image = imagArr[indexPath.item]
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ServiceCell.cellId, for: indexPath) as! ServiceCell
            if indexPath.item == 0{
                cell.label.text = "Send Packages"
                cell.catImg.image = UIImage(named: "sendpackage")
            }else{
                cell.label.text = "Purchase From Store"
                cell.catImg.image = UIImage(named: "storepickup")
            }
            
            return cell
        default:
           // break
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.cellId, for: indexPath) as! CategoryCell
            cell.catImg.image = UIImage(named: "daburicon")
            cell.label.text = ["Food Delhivery", "Fruits & Vegitables", "Grocery", "Meat & Chiken","Dairy Products","The royale Birayani house","Badastoor DAsterkhawan"][indexPath.item]
           // cell.backgroundColor = .blue
            cell.layoutIfNeeded()
            return cell
        }
       
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ProfileTabVC()
                    let v = parent as! UINavigationController
                    let sp = v.parent as! TabViewController
                    sp.hideTopBarWhenPushed(true)
        
        sp.hidesBottomBarWhenPushed = true
        sp.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //view.backgroundColor = .red
        self.navigationController?.navigationBar.isHidden = true
        collectionView.backgroundColor = .systemBackground
        collectionView.showsVerticalScrollIndicator = false
        navigationItem.title = "TITLE"
        collectionView.register(BannerCell.self, forCellWithReuseIdentifier: BannerCell.cellId)
        collectionView.register(CategoryCell.self, forCellWithReuseIdentifier: CategoryCell.cellId)
        collectionView.register(ServiceCell.self, forCellWithReuseIdentifier: ServiceCell.cellId)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(HEader.self, forSupplementaryViewOfKind: HomeTabVC.catgoryld, withReuseIdentifier: headerId)
        collectionView.register(Footer.self, forSupplementaryViewOfKind: HomeTabVC.footerPAgeControl,withReuseIdentifier: footerid)
        collectionView.contentInsetAdjustmentBehavior = .never
        
        
        ApiCall.shared.getserviceBannerImage { statuscode, dict in
           // print(dict)
            let message = dict["message"]
            let data = dict["data"]
           // print(message,data)
            for d in data as! [[String:Any]]{
                self.bannerimages.append(d["imageUrl"] as! String)
                let item = d["id"] as! Int
                
                self.downloadImage(from: URL(string:d["imageUrl"] as! String)!, id: item-1)
            }
            
            
        }
       
        
        
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    func downloadImage(from url: URL,id:Int) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            self.imagArr.append(UIImage(data: data)!)
            DispatchQueue.main.async() { [weak self] in
                self!.collectionView.reloadItems(at: [IndexPath(item: id, section: 0)])
            }
        }
    }
    var imagArr = [UIImage]()
    var bannerimages = [String]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       startTimer()
       
        let v = parent as! UINavigationController
        let sp = v.parent as! TabViewController
        sp.hideTopBarWhenPushed(false)
       // self.tabBarController?.tabBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        incre = 0
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setuptest()
    }
    //MARK:- function for setting view frame
    func  setuptest(){
//        view.addSubview(labelOne)
//        labelOne.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        labelOne.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let window = windowScene?.windows.first
      
        let topPadding = window!.safeAreaInsets.top
        self.view.frame.origin.y = CGFloat(topPadding + 70)
       
        //self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
        
        //            let v = parent as! UINavigationController
        //            let sp = v.parent as! MyTabBarCtrl
        //            sp.hideTopBarWhenPushed(true) while push
       
    }
    
    
    let labelOne:UILabel = {
            let label = UILabel()
        //label.backgroundColor = .green//Constants.Design.Color.Primary.v3txtfFontCol
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)//Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "1"
        label.numberOfLines = 1
        label.textAlignment = .center
       // label.sizeToFit()
            return label
    }()
  
    var timer: Timer?
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.6, target: self, selector: #selector(scrollAutomatically), userInfo: nil, repeats: true)
    }

    var incre = 0
    @objc func scrollAutomatically(_ timer1: Timer) {
       
            let indexPath = IndexPath(item: incre, section: 0)//collectionView.indexPath(for: cell)!
       
            if incre <= 2 {
                
                let indexPath1 = IndexPath.init(row: indexPath.row + 1, section: indexPath.section)
                if collectionView.isCellVisible(indexPath: indexPath){
                collectionView.scrollToItem(at: indexPath1, at: .right, animated: true)
                incre = indexPath1.item
                
                let f = collectionView.supplementaryView(forElementKind: HomeTabVC.footerPAgeControl, at: IndexPath(item: 0, section: 0)) as! Footer
                
                    f.pageControl.currentPage = indexPath1.row}
            }
            else  {
              
               
                
                let checkIndex = IndexPath.init(row: incre == 3 ? 2 : 1, section: indexPath.section)
                if collectionView.isCellVisible(indexPath: checkIndex){
                let ind = incre == 3 ? 1 : 0
                incre = ind == 1 ? 4 : 0
                let indexPath1 = IndexPath.init(row: ind, section: indexPath.section)
               
                collectionView.scrollToItem(at: indexPath1, at: .left, animated: true)
               
                let f = collectionView.supplementaryView(forElementKind: HomeTabVC.footerPAgeControl, at: IndexPath(item: 0, section: 0)) as! Footer
                
                f.pageControl.currentPage = ind
                
                }
            }
        
    }
}

class HEader:UICollectionReusableView{
    let label:UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18.0)
        return l
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.text = "category"
    addSubview(label)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = bounds
    }
}
class Footer:UICollectionReusableView{
   // let label = UILabel()
    
    
    let pageControl:UIPageControl = {
        let p = UIPageControl()
       
        p.translatesAutoresizingMaskIntoConstraints = false
        
        return p
    }()
   
    override init(frame: CGRect) {
        super.init(frame: frame)
//
//        label.text = "category"
//    addSubview(label)
       // pageControl.backgroundColor = .blue
        addSubview(pageControl)
      
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //label.frame = bounds
        pageControl.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        pageControl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        pageControl.widthAnchor.constraint(equalToConstant: 200).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        pageControl.pageIndicatorTintColor = .black
        pageControl.currentPageIndicatorTintColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)//.yellow
        pageControl.tintColor = .purple
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
       
    }
}
extension UICollectionView {
    func isCellVisible(indexPath: IndexPath)-> Bool {
         let indexs = self.indexPathsForVisibleItems
        return indexs.contains(indexPath)
    }
}
