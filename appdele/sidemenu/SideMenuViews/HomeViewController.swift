//
//  HomeViewController.swift
//  appdele
//
//  Created by Danish computer technologies on 24/02/22.
//

import UIKit
import CoreLocation
extension TabViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
          ///https://fluffy.es/current-location/
            print("Lat : \(location.coordinate.latitude) \nLng : \(location.coordinate.longitude)")
            locationManager?.stopUpdatingLocation()
            getReverSerGeoLocation(location: CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        }
    }
    func getReverSerGeoLocation(location : CLLocation) {
        print("getting Address from Location cordinate")

        CLGeocoder().reverseGeocodeLocation(location) {
            placemarks , error in
        
            if error == nil && placemarks!.count > 0 {
                guard let placemark = placemarks?.last else {
                    return
                }
                print(placemark.thoroughfare)
                print(placemark.subThoroughfare)
                print("postalCode :-",placemark.postalCode)
                print("City :-",placemark.locality)
                print("subLocality :-",placemark.subLocality)
                print("subAdministrativeArea :-",placemark.subAdministrativeArea)
                print("Country :-",placemark.country)
            }
        }
    }

}
class TabViewController: UITabBarController, UITabBarControllerDelegate{
    var safeArea: UILayoutGuide!
    override func loadView() {
      super.loadView()
        view.backgroundColor = .secondarySystemBackground//Constants.Design.Color.Primary.v3superbackgroundCol
      safeArea = view.layoutMarginsGuide
      
    }
//    open override var tabBar: TabBar {
//            return TabBar()
//        }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Menu Button Tint Color
       // navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .systemBackground//white//systemPink
        setValue(TabBar(frame: tabBar.frame), forKey: "tabBar")
        
       // self.navigationController?.navigationBar.isTranslucent = false
       setupTopView()
       
        
       setupchilds()
        
        
        
    }
    private var locationManager:CLLocationManager?
    func getUserLocation() {
           locationManager = CLLocationManager()
           locationManager?.requestAlwaysAuthorization()
           locationManager?.delegate = self
           locationManager?.startUpdatingLocation()
       }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
      
      
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.revealViewController()?.gestureEnabled = false
        hideTopBarWhenPushed(false)
        if let tabItems = self.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            tabItem.badgeValue = "1"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // self.revealViewController()?.gestureEnabled = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getUserLocation()
        
    }
    func setupchilds(){
        self.delegate = (self as UITabBarControllerDelegate)
        
        tabBar.tintColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)//.black//.systemTeal
        
        let home = HomeTabVC()
        home.tabBarItem.image = UIImage(named: "tabhome")!//.withRenderingMode(.alwaysOriginal)
        //home.tabBarItem.selectedImage = UIImage(systemName: "house.fill")!
        let navHome = UINavigationController(rootViewController: home)
        //addChild(navHome)
        let services:UIViewController = ServicesTabVC()
       
        
        //services.tabBarItem.title = "Calendar"
        services.tabBarItem.image = UIImage(named: "tabservice")!//.withRenderingMode(.alwaysOriginal)
        //services.tabBarItem.selectedImage = UIImage(systemName: "command")!//.withRenderingMode(.alwaysOriginal)
        let navservice = UINavigationController(rootViewController: services)
        
        
        let statuS:UIViewController = StatusTabVC()
        let navstatus = UINavigationController(rootViewController: statuS)
        //addChild(navstatus)
        //navigationControllerc.interactivePopGestureRecognizer?.isEnabled = true
        // chat room test
//        CustomUICollectionViewFlowLayout * layout = [[CustomUICollectionViewFlowLayout alloc] init];
//        ChatBoatController *notification = [[ChatBoatController alloc] initWithCollectionViewLayout: layout];
       // let layout = UICollectionViewFlowLayout()//CustomUICollectionViewFlowLayout()
        //let chat = ChatRoomController(collectionViewLayout: layout)//ChatBoatController(collectionViewLayout: layout)
        //let window = UIApplication.shared.keyWindow
        //let topPadding = window!.safeAreaInsets.top
       // let bottomPadding = window!.safeAreaInsets.bottom
       // print(topPadding,bottomPadding) height = height < bounds.height ? bounds.height : height
      
       // home.tabBarItem.title = "Home"
       // home.tabBarItem.image = UIImage(systemName: "house")!//.withRenderingMode(.alwaysOriginal)
       // home.tabBarItem.selectedImage = UIImage(systemName: "house")!//.withRenderingMode(.alwaysOriginal)
        
//        if bottomPadding > 0{
//            home.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
//           home.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -20)
//
//            services.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
//            services.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -20)
//        }
  
        //statuS.tabBarItem.title = "Chat"
        statuS.tabBarItem.image = UIImage(named: "tabtracking")!//.withRenderingMode(.alwaysOriginal)
       // statuS.tabBarItem.selectedImage = UIImage(systemName: "arrow.swap")!//.withRenderingMode(.alwaysOriginal)
//        if bottomPadding > 0{
//            statuS.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
//            statuS.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -20)
//            }
//
        
        
        let cart = CartTabVC()
        
        let navigationControllerSettting = UINavigationController(rootViewController: cart)
        //cart.tabBarItem.title = "Settings"
        cart.tabBarItem.image = UIImage(named: "tabcart") //Settingv3s
        //cart.tabBarItem.selectedImage = UIImage(systemName: "cart")!//.withRenderingMode(.alwaysOriginal)
//        if bottomPadding > 0{
//            cart.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
//            cart.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -20)}
        
        let test = ProfileTabVC()//UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FolderViewController") as! FolderViewController
        test.tabBarItem.title = nil//"test"
        test.tabBarItem.image = UIImage(named: "tabprofile")
            viewControllers = [navHome,navservice,navstatus,navigationControllerSettting,test]
        
//        if let arrayOfTabBarItems = self.tabBar.items as AnyObject as? NSArray,let
//                 tabBarItem = arrayOfTabBarItems[4] as? UITabBarItem {
//                 tabBarItem.isEnabled = false
//              }
        
    }
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
       // print("Selected Index ye tab bar :\(self.selectedIndex),\(item.title)");
//        if item.title == "Chat"{
//            //tabBar.items?[1].title = "Create Chat"
//
//        }
//        else if item.title == "Task"{
//
//           // tabBar.items?[1].title = "Create Folder"
//        }
       /* else if item.title == "New Chat"{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectPersonForChatVC") as! SelectPersonForChatVC
                let navigationControllerc = UINavigationController(rootViewController: vc)
                //vc.viewFor = "invite"
                //vc.delegate = self
                //isCollaborate = true
            navigationControllerc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(navigationControllerc, animated: true, completion: nil)
            
        }*/
        
    }
    
    // top view view with shadow child view label button button searchbar on top of
    func setupTopView(){
        
        /*
       
       // sideMenuBtn.target = revealViewController()
       // sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        
        view.addSubview(sideMenuBtn)
         sideMenuBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 16).isActive = true
         sideMenuBtn.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
         */
        
        view.addSubview(topView)
        view.addSubview(searchView)
        topView.addSubview(sideMenuBtn)
        topView.addSubview(locationLbl)
        topView.addSubview(topCartBtn)
        
        
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let window = windowScene?.windows.first
       // let window = UIApplication.shared.windows.first

       let topPadding = window!.safeAreaInsets.top
       //let bottomPadding = window!.safeAreaInsets.bottom
        //topView.alpha = 0.4
        
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 70+topPadding).isActive = true
        
        sideMenuBtn.leadingAnchor.constraint(equalTo: topView.leadingAnchor,constant: 8).isActive = true
        sideMenuBtn.topAnchor.constraint(equalTo: topView.topAnchor,constant: topPadding).isActive = true
        sideMenuBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        sideMenuBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        topCartBtn.centerYAnchor.constraint(equalTo: sideMenuBtn.centerYAnchor).isActive = true
        topCartBtn.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -8).isActive = true
        topCartBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        locationLbl.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
        locationLbl.topAnchor.constraint(equalTo: sideMenuBtn.firstBaselineAnchor).isActive = true
        locationLbl.leadingAnchor.constraint(equalTo: sideMenuBtn.trailingAnchor, constant: 8).isActive = true
        locationLbl.trailingAnchor.constraint(equalTo: topCartBtn.leadingAnchor, constant: -16).isActive = true
        
        searchView.centerYAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        searchView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        searchView.leadingAnchor.constraint(equalTo: topView.leadingAnchor,constant: 24).isActive = true
        searchView.trailingAnchor.constraint(equalTo: topView.trailingAnchor,constant: -24).isActive = true
        
        sideMenuBtn.addTarget(revealViewController(), action: #selector(revealViewController()?.revealSideMenu), for: .touchUpInside)
        
        topCartBtn.addBadge(number: 99)//(number: 8, withOffset: CGPoint(x: 10, y: 0), andColor: .blue, andFilled: true)
        
       
        
    }
        func hideTopBarWhenPushed(_ action:Bool){
            topView.isHidden = action
            searchView.isHidden = action
        }
    let topView:UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        v.layer.cornerRadius = 20
      return v
    }()
//    let searchView:UIView = {
//        let v = UIView()
//        v.translatesAutoresizingMaskIntoConstraints = false
//        v.backgroundColor = .orange
//      return v
//    }()
    lazy var searchView:UITextField = {
            let tf = UITextField()
            tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
           // tf.textColor = Constants.Design.Color.Primary.v3txtfFontCol
            tf.placeholder = "Search here"
            tf.backgroundColor = .white//Constants.Design.Color.Primary.v3superbackgroundCol
            tf.layer.cornerRadius = 10
            //tf.delegate = self
        tf.borderStyle = .none//UITextField.BorderStyle.roundedRect
       // tf.layer.borderColor = UIColor.gray.cgColor
        //tf.layer.borderWidth = 1
        //tf.layer.cornerRadius = 8
            tf.clearsOnBeginEditing = true
            //tf.addlabel(str: "", w: 10)
            tf.autocorrectionType = .no
            tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.shadowOpacity = 0.5
        tf.layer.shadowRadius = 3.0
        tf.layer.shadowOffset = CGSize(width: 0, height: 1)//CGSize.zero // Use any CGSize
        tf.layer.shadowColor = UIColor.gray.cgColor
        
//        tf.leftViewMode = UITextField.ViewMode.always
//        let imageView = UIImageView(frame: CGRect(x: 8, y: 0, width: 20, height: 20))
        let image = UIImage(named: "search")
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        tf.leftView = imageView
        setIcon(image!, w: 40, x: 12, tf: tf)
            return tf
        }()
    let locationLbl:UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.text = "216 zone 1 MP Nagar bhopal 462003 in front of multi level smart parking"
        v.textColor = .darkGray
        v.numberOfLines = 2
        v.textAlignment = .center
        v.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 12.0)
      return v
    }()
    
    let sideMenuBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "menu"), for: .normal)//setTitle("SideMenu", for: .normal) //menucard
        button.tintColor = .black
       // button.setTitleColor(.white, for: .normal)
        //button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.layer.cornerRadius = 3
       // button.backgroundColor = UIColor.lightGray
      
        return button
    }()
    let topCartBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "cart"), for: .normal)//setTitle("cart", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 3
        //button.backgroundColor = UIColor.lightGray
        button.tintColor = .black
        return button
    }()
    
}
func setIcon(_ image: UIImage,w:CGFloat,x:CGFloat? = 10,squareof:CGFloat? = 20,tf:UITextField) {
   let iconView = UIImageView(frame:
                  CGRect(x: x!, y: 5, width: squareof!, height: squareof!))
   iconView.image = image
   let iconContainerView: UIView = UIView(frame:
                  CGRect(x: 0, y: 0, width: w, height: squareof!+10)) // was 45
   iconContainerView.addSubview(iconView)
    tf.leftView = iconContainerView
    tf.leftViewMode = .always
    // application with tint
//        @IBOutlet weak var iconTextField: UITextField! {
//           didSet {
//              iconTextField.tintColor = UIColor.lightGray
//              iconTextField.setIcon(imageLiteral(resourceName: "icon-user"))
//           }
//        }
}

@IBDesignable
class TabBar: UITabBar {
    private var cachedSafeAreaInsets = UIEdgeInsets.zero

            let keyWindow = UIApplication.shared.connectedScenes
                .filter { $0.activationState == .foregroundActive }
                .compactMap { $0 as? UIWindowScene }
                .first?.windows
                .filter { $0.isKeyWindow }
                .first

            override var safeAreaInsets: UIEdgeInsets {
                if let insets = keyWindow?.safeAreaInsets {
                    if insets.bottom < bounds.height {
                        cachedSafeAreaInsets = insets
                    }
                }
                return cachedSafeAreaInsets
            }

        override open func sizeThatFits(_ size: CGSize) -> CGSize {
               super.sizeThatFits(size)
            let scenes = UIApplication.shared.connectedScenes
            let windowScene = scenes.first as? UIWindowScene
            let window = windowScene?.windows.first
           // let window = UIApplication.shared.windows.first
    
           // let topPadding = window!.safeAreaInsets.top
            let bottomPadding = window!.safeAreaInsets.bottom

               var sizeThatFits = super.sizeThatFits(size)
           // print(sizeThatFits.height,topPadding,bottomPadding)
            sizeThatFits.height += bottomPadding == 0 ? 11 : 0
               return sizeThatFits
           }
    private var shapeLayer: CALayer?
    private func addShape() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0.0).cgPath
        if #available(iOS 13.0, *) {
            shapeLayer.strokeColor = UIColor.blue.cgColor//Constants.Design.Color.Primary.v3BlueWithOpacity.cgColor
        } else {
            // Fallback on earlier versions
        }
        shapeLayer.frame = self.bounds
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 0.0
        
        //The below 4 lines are for shadow above the bar. you can skip them if you do not want a shadow
        shapeLayer.shadowOffset = CGSize(width:0, height:0)
        shapeLayer.shadowRadius = 4
        shapeLayer.shadowColor = UIColor.gray.cgColor//Constants.Design.Color.Primary.v3Blue.cgColor
        shapeLayer.shadowOpacity = 1//0.3

        if let oldShapeLayer = self.shapeLayer {
            self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        } else {
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
        self.shapeLayer = shapeLayer
    }
    override func draw(_ rect: CGRect) {
  
        self.addShape()
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard !clipsToBounds && !isHidden && alpha > 0 else { return nil }
        for member in subviews.reversed() {

            let subPoint = member.convert(point, from: self)
            guard let result = member.hitTest(subPoint, with: event) else { continue }
            return result
        }
        return nil
    }
    //var height:CGFloat = 0
    func createPath() -> CGPath {
       // let height: CGFloat = 80.0
        let guide = self.safeAreaLayoutGuide
        var height = guide.layoutFrame.size.height
       
        height = height < bounds.height ? bounds.height : height
        //print(bounds.height,height)
        
        let path = UIBezierPath()
       
//        path.move(to: CGPoint(x: 0, y: height)) // start bottom left
//
//
//        path.addCurve(to: CGPoint(x: 70, y: 0),
//        controlPoint1: CGPoint(x: 0, y: 0), controlPoint2: CGPoint(x: 0, y: 0))
//
//         path.addLine(to: CGPoint(x: self.frame.width - 70, y: 0))
//
//
//        path.addCurve(to: CGPoint(x: self.frame.width, y: height),
//        controlPoint1: CGPoint(x: self.frame.width, y: 0), controlPoint2: CGPoint(x: self.frame.width, y: 0))
//
//        path.close()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 1, y: 1))
        path.addLine(to: CGPoint(x: 1, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 1))
        path.close()

        return path.cgPath
    }
}

//https://stackoverflow.com/questions/34429534/dynamic-uitableview-height/34430475
//http://www.matthewhsingleton.com/coding-with-a-rubber-ducky/2016/5/26/predictive-text-table-view-swift-version-of-ray-wenderlich

//https://www.freecodecamp.org/news/how-to-create-an-autocompletion-uitextfield-using-coredata-in-swift-dbedad03ea3d/

// MARK:- badge button using cashpelayer extension and button extension-
extension CAShapeLayer {
    func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
}

private var handle: UInt8 = 0

extension UIButton {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }

    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
       // guard let view = self.value(forKey: "view") as? UIView else { return }
       
        badgeLayer?.removeFromSuperlayer()
       
        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(8)
        let location = CGPoint(x: /*self.frame.width - (radius + offset.x)*/20, y: 5/*(radius + offset.y)*/)
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        self.layer.addSublayer(badge)

        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = "\(number)"
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.fontSize = 10
        label.frame = CGRect(origin: CGPoint(x: location.x - radius, y: offset.y-2), size: CGSize(width: 16, height: 16))
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)

        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }

    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}
