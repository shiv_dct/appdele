//
//  test.swift
//  appdele
//
//  Created by Danish computer technologies on 22/02/22.
//

import UIKit
class test:UIViewController{
    override func viewDidLoad() {
     super.viewDidLoad()
        view.backgroundColor = .yellow
        setupTopView()
        setupTextFields()
    }
    
    let topView: UIView = {
            let view = UIView()
            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
    
    func setupTopView() {
            
            view.addSubview(topView)
            
            topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            topView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        }
    let emailTextField: UITextField = {
            let tf = UITextField()
            tf.translatesAutoresizingMaskIntoConstraints = false
            tf.placeholder = "Email"
            tf.borderStyle = .roundedRect
            tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
        tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
            return tf
            
        }()
        
        let usernameTextField: UITextField = {
            let tf = UITextField()
            tf.translatesAutoresizingMaskIntoConstraints = false
            tf.placeholder = "Username"
            tf.borderStyle = .roundedRect
            tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
            tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
            return tf
            
        }()
        
        let passwordTextField: UITextField = {
            let tf = UITextField()
            tf.translatesAutoresizingMaskIntoConstraints = false
            tf.placeholder = "Password"
            tf.isSecureTextEntry = true
            tf.borderStyle = .roundedRect
            tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
            tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
            return tf
            
        }()
        
        let signUpButton: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle("Sign Up", for: .normal)
            button.setTitleColor(.white, for: .normal)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.layer.cornerRadius = 3
            button.backgroundColor = UIColor.lightGray
            button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
            return button
        }()
    func setupTextFields() {
            
            let stackView = UIStackView(arrangedSubviews: [emailTextField, usernameTextField, passwordTextField, signUpButton])
            stackView.axis = .vertical
            stackView.spacing = 10
            stackView.distribution = .fillEqually
            stackView.translatesAutoresizingMaskIntoConstraints = false
            
            //add stack view as subview to main view with AutoLayout
            view.addSubview(stackView)
            stackView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 40).isActive = true
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
            stackView.heightAnchor.constraint(equalToConstant: 200).isActive = true
            
        }
    
    @objc func handleSignUp() {
           validateForm()
       }
       
       func validateForm() {
           guard let emailText = emailTextField.text, !emailText.isEmpty else { return }
           guard let passwordText = passwordTextField.text, !passwordText.isEmpty else { return }
           guard let usernameText = usernameTextField.text, !usernameText.isEmpty else { return }
           
           startSigningUp(email: emailText, password: passwordText, username: usernameText)
       }
       
       func startSigningUp(email: String, password: String, username: String) {
           print("Please call any Sign up api for registration: ", email, password, username)
       }
    @objc func handleTextChange() {
            
            let emailText = emailTextField.text!
            let usernameText = usernameTextField.text!
            let passwordText = passwordTextField.text!
            
            let isFormFilled = !emailText.isEmpty && !usernameText.isEmpty && !passwordText.isEmpty
            
            if isFormFilled {
                signUpButton.backgroundColor = UIColor.orange
                signUpButton.isEnabled = true
            }else {
                signUpButton.backgroundColor = UIColor.lightGray
                signUpButton.isEnabled = false
            }
            
        }
    
    let topNameLabel:UILabel = {
            let label = UILabel()
       // label.backgroundColor = Constants.Design.Color.Primary.v3txtfFontCol
       // label.font = Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        //label.text = "Reminder set"
        label.numberOfLines = 1
       // label.sizeToFit()
            return label
    }()
    let topImgV:UIImageView = {
             let img = UIImageView()
             //img.backgroundColor = Constants.Design.Color.Primary.v3Lightblue
             img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
             img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
             img.layer.cornerRadius = 17.5
           // img.image = UIImage(named: "pdf")
             img.clipsToBounds = true
       
            return img
         }()

}
