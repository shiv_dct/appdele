//
//  AppDelegate.swift
//  appdele
//
//  Created by Danish computer technologies on 22/02/22.
//
/*
 use scene and app dele or uikit and swiftui in one project refrence link :-
 
 https://www.fivestars.blog/articles/app-delegate-scene-delegate-swiftui/
 */
/*
 understanding app delgate and scene delegate
 https://medium.com/@salehmasum_9553/creating-login-registration-form-with-swift-without-using-storyboard-8a259cdc9ac5
 */
/*
    1. Remove Scene delegate methods from App Delegate and delete the Scene delegate file.
    2. add var window:UIWindow?
    3. Remove SceneDelegate.swift file
       Remove Application Scene Manifest from Info.plist file
       Paste this code to your AppDelegate.swift file
 
    var window:UIWindow?
 var window:UIWindow?

 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  storyboard starting point hai
     return true
 }

   
 
 */


/*
 if you want remove mainstory board
 target deployemnt info main interface empty karo
 delete storyboard file
 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
     self.window = UIWindow(frame: UIScreen.main.bounds)
     window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
     window?.makeKeyAndVisible()

     return true
 }
 */

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window:UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
     
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.overrideUserInterfaceStyle = .light
        setRootViewController()
//
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor.systemTeal], for: .selected)
       // UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -6)
        
       /* window?.rootViewController = UINavigationController(rootViewController: RootLoginVC())//RootLoginVC()//RootTabSidemenuVc()//test()//UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        
        window?.makeKeyAndVisible()*/
       // UITabBar.appearance().backgroundColor = .white//UIColor(red:1, green:0, blue:0, alpha:1)
        return true
    }
    
    
    func setRootViewController(){
       
       
        //change status bar color statuscbar extension mai banaya hai
        UIApplication.shared.statusBarUIView?.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        let userDefaults = UserDefaults.standard
        if userDefaults.value(forKey: "login") != nil{
            window?.rootViewController = UINavigationController(rootViewController: RootShiViewController())
            
            window?.makeKeyAndVisible()
        }else{
            window?.rootViewController = UINavigationController(rootViewController: RootLoginVC())
            
            window?.makeKeyAndVisible()
        }
        
//        let decoded  = UserDefaults.standard.object(forKey: "userDataemail") as! Data
//        let decodedTeams = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? [String:Any]
//        print(decodedTeams)
    }

    

}

extension UIApplication {
var statusBarUIView: UIView? {

    if #available(iOS 13.0, *) {
        let tag = 3848245

        let keyWindow = UIApplication.shared.connectedScenes
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows.first

        if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {
            let height = keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
            let statusBarView = UIView(frame: height)
            statusBarView.tag = tag
            statusBarView.layer.zPosition = 999999

            keyWindow?.addSubview(statusBarView)
            return statusBarView
        }

    } else {

        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
    }
    return nil
  }
}
