////  https://learnappmaking.com/urlsession-swift-networking-how-to/
// https://www.hackingwithswift.com/example-code/networking/how-to-make-a-network-request-wait-for-an-internet-connection-using-waitsforconnectivity
//  https://useyourloaf.com/blog/urlsessionconfiguration-quick-guide/
//  DataAPI.swift
//  appdele
//
//  Created by shivendra singh on 24/02/22.
// network handler

import UIKit



//import Foundation
//import UIKit
class DataApiService: NSObject {
    
    static let shared = DataApiService()
   // let BASE_URL  = "https://www.playdiator.com/workon/" // prod
    let BASE_URL  = "https://service.doosy.in/doosy/"
  


    func postMethod(url: String ,parameters: [String: Any] ,  completion: @escaping(Bool , [String: Any]) -> Void){
        //create the url with URL
        let url = URL(string: "\(BASE_URL)\(url)")! //change the url
        let para = parameters//["data":parameters]
        
        //create the session object
        let session = URLSession.shared
       
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        
        
       
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: para, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            //print("2shiv\(error)***********\(response)***********\(data)")
            guard error == nil else {
                if error?._code == -1001 {
                           //Domain=NSURLErrorDomain Code=-1001 "The request timed out."
                    completion(false , ["Error" : "Domain=NSURLErrorDomain Code=-1001 The request timed out."])
                          }
                completion(false , ["err" : "error"])
                return
            }
            
            guard let data = data else {
                completion(false,["" : ""])
                return
            }
            
            do {
                //create json object from data
                if let returndict = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    completion(true,returndict )
                }
            } catch let error {
                print(error.localizedDescription)
                completion(false , ["" : ""])
            }
        })
        task.resume()
    }

   
    /*
    Banner
    Query String Parameters
    imageType: Banner
    https://service.doosy.in/doosy/slider/list?imageType=Banner
    GET /doosy/slider/list HTTP/1.1*/
    func queryMethod(url: String, parameters: [String:String], completion: @escaping ([String: Any]?, Error?) -> Void) {
        //create the url with URL
        let url = "\(BASE_URL)\(url)"
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let request = URLRequest(url: components.url!)
       
        
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
           // print("3shiv\(error)")
            guard let data = data,                            // is there data
                let response = response as? HTTPURLResponse,  // is there HTTP response
                (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                error == nil else {                           // was there no error, otherwise ...
                    completion(nil, error)
                    return
            }
            
            let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
            completion(responseObject, nil)
        }
        task.resume()
    }
    
    func getMethod(url: String,  completion: @escaping(Bool , [String: Any]) -> Void) {
        
        //create the url with NSURL
        let url = URL(string: "\(BASE_URL)\(url)")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
           // print("1shiv\(error)")
            guard error == nil else {
                completion(false , ["" : ""])
                
                return
            }
            
            guard let data = data else {
                completion(false , ["" : ""])
                
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    completion(true,json )
                   // print(json)
                }
            } catch let error {
                print(error.localizedDescription)
                completion(false , ["" : ""])
            }
        })
        task.resume()
    }

    let register_URL = "customer"
    func register_User(parameters: [String: Any],completion: @escaping(Bool , [String: Any]) -> Void){
        postMethod(url: register_URL, parameters: parameters) { statuscode, response in
           // print(statuscode,response)// true ["message": Same phone number exist for different customer!! Kindly use different number., "status": 400]
            completion(statuscode,response)
        }

    }

    let Forgot_URL = "user/login/forgotPassword"
    func forgot_email_password(parameters: [String: Any],completion: @escaping(Bool , [String: Any]) -> Void){
        postMethod(url: Forgot_URL, parameters: parameters) { statuscode, response in
            //print(response)
            completion(statuscode,response)
        }

    }
    
    let Login_URL = "user/login/customer/login"
    func login_email_User(parameters: [String: Any],completion: @escaping(Bool , [String: Any]) -> Void){
        postMethod(url: Login_URL, parameters: parameters) { statuscode, response in
            //print(response)
            completion(statuscode,response)
        }

    }
    
    
    func get_otp(number:String,completion: @escaping(Bool , [String: Any]) -> Void){
        let url = "user/login/customer/generate/otp/\(number)"
        getMethod(url: url) { sucess, response in
            completion(sucess,response)
        }
    }
    
    func serviceBanner(completion: @escaping(Bool,[String:Any])-> Void){
        let parameters = ["imageType": "Banner"]
        queryMethod(url: "slider/list", parameters: parameters) { sucessdata, err in
            if err == nil{
                completion(true,sucessdata!)
                
            }else{
                completion(false,["error":err as Any])
                }
        }
      
    }
    
   
 }




class ApiCall : NSObject{
    
    static let shared = ApiCall()
    /*
     true ["data": {
         active = 1;
         addressList = "<null>";
         birthDate = "<null>";
         createdAt = "2022-03-09T09:23:41.852+00:00";
         email = "tryh@rjf.com";
         emailVerified = 0;
         firstName = Test;
         gender = "<null>";
         id = 1831;
         lastName = Tr;
         name = "Test Tr";
         phoneNumber = 4563728274;
         phoneVerified = 0;
         registeredVia = APP;
         status = Pending;
         userId = 2181;
         walletAmt = 0;
     }, "message": Customer created successfully, "status": 200]
     */
    func resultCheck(result :Bool ,dict:[String:Any] , completion: @escaping(Bool , [String: Any]?) -> Void){
        if let respose : NSDictionary = dict["data"] as? NSDictionary{
            if let code : Int = dict["status"] as? Int{
                if code == 200{
                    completion(true,respose as? [String : Any])
                }
                else{
                    completion(false, ["message":dict["message"] as! String])
                }
            }
            else{
                completion(false,["message":dict["message"] as! String])
            }
        }else{
            
            if let code : Int = dict["status"] as? Int{
                if code == 200{
                    completion(true,dict)
                }
                else{
                    completion(false, ["message":dict["message"] as! String])
                }
            }
            else{
                completion(false,["message":dict["message"] as! String])
            }
        
        }
    }
    
    
   // {"firstName":"test","lastName":"user","phoneNumber":"1234567890","email":"testuser@gmail.com","password":"12345678","confirmPassword":"12345678","acceptTnC":true,"registeredVia":"APP","active":true}
   
    func register_new_User(firstName:String,lastName:String,phoneNumber:String,email:String,password:String,confirmPassword:String,registeredVia:String,acceptTnC:Bool,active:Bool, completion: @escaping(Bool,[String: Any]) -> Void){
        let passingData : [String : Any] = ["firstName" : firstName ,"lastName" : lastName,"phoneNumber" : phoneNumber ,"password" : password,"email" : email ,"confirmPassword" : confirmPassword,"acceptTnC" : acceptTnC ,"registeredVia" : registeredVia,"active" : active]
        if Reachability.isConnectedToNetwork(){
            
     
            DataApiService.shared.register_User(parameters: passingData) { (result, dic) in
               // print(dic)
                if result{
                    self.resultCheck(result: result, dict: dic) { sucess, response in
                        if sucess{
                            completion(sucess,response!)
                        }else{
                            // message fail wala bhejo controller ko
                            completion(sucess,response!)
                        }
                    }
                   
                }else{
                    // failed from network
                    //print(dic)
                    completion(false, dic)
                }
                
                
                
                
            }
            
        }else{
            //print("Internet Connection not Available!")
            completion(false,["message":"internet not available"])
        }
       
    
        }
    
    func login_email_User(email:String,password:String, completion: @escaping(Bool,[String: Any]) -> Void){
        let passingData : [String : Any] = ["email" : email ,"password" : password]
        if Reachability.isConnectedToNetwork(){
             //let fileUrl = URL(string: user.profilePic)
     
            DataApiService.shared.login_email_User(parameters: passingData) { (result, dic) in
                
                if result{
                    self.resultCheck(result: result, dict: dic) { sucess, response in
                        if sucess{
                            completion(sucess,response!)
                        }else{
                            // message fail wala bhejo controller ko
                            completion(sucess,response!)
                        }
                    }
                }else{
                    completion(false, dic)
                }
            }
            
        }else{
            //print("Internet Connection not Available!")
            completion(false,["message":"internet not available"])
        }
       
    
        }
    
    func login_mobile_User(mobile:String,otp:String, completion: @escaping(Bool,[String: Any]) -> Void){
        let passingData : [String : Any] = ["phoneNumber" : mobile ,"otp" : otp]
        if Reachability.isConnectedToNetwork(){
             //let fileUrl = URL(string: user.profilePic)
     
            DataApiService.shared.login_email_User(parameters: passingData) { (result, dic) in
                
                if result{
                    self.resultCheck(result: result, dict: dic) { sucess, response in
                        if sucess{
                            completion(sucess,response!)
                        }else{
                            // message fail wala bhejo controller ko
                            completion(sucess,response!)
                        }
                    }
                }else{
                    completion(false, dic)
                }
            }
            
        }else{
            //print("Internet Connection not Available!")
            completion(false,["message":"internet not available"])
        }
       
    
        }
    
    //{"email":"zeedeespeedy@gmail.com","userType":"CUSTOMER","type":"EMAIL","sendingType":"BOTH"}
    func forgot_password(email:String,userType:String,type:String,sendingType:String, completion: @escaping(Bool,[String: Any]) -> Void){
        let passingData : [String : Any] = ["email" : email ,"userType" : userType,"type" : type ,"sendingType" : sendingType]
        if Reachability.isConnectedToNetwork(){
            
     
            DataApiService.shared.forgot_email_password(parameters: passingData) { (result, dic) in
                
                if result{
                    self.resultCheck(result: result, dict: dic) { sucess, response in
                        if sucess{
                            completion(sucess,response!)
                        }else{
                            // message fail wala bhejo controller ko
                            completion(sucess,response!)
                        }
                    }
                }else{
                    completion(false, dic)
                }
            }
            
        }else{
            print("Internet Connection not Available!")
            completion(false,["message":"internet not available"])
        }
       
    
        }
    
    func genrateOTPForNumber(number:String,completion: @escaping(Bool,[String: Any]) -> Void){
        if Reachability.isConnectedToNetwork(){
            DataApiService.shared.get_otp(number: number) { sucess, response in
                if sucess{
                   // print(dic)
                    completion(sucess, response)
                }else{
                    completion(sucess, response)
                }
            }
        }else{
            
        }
    }
        
    func getserviceBannerImage(completion: @escaping(Bool,[String: Any]) -> Void){
        
        if Reachability.isConnectedToNetwork(){
             //let fileUrl = URL(string: user.profilePic)
     
            DataApiService.shared.serviceBanner{ (result, dic) in
                
                if result{
                   // print(dic)
                    completion(result, dic)
                }else{
                    completion(false, dic)
                }
            }
            
        }else{
            print("Internet Connection not Available!")
        }
       
    
        }
    
    
}


//-----------

//import Foundation
import SystemConfiguration

public class Reachability {

    class func isConnectedToNetwork() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        /* Only Working for WIFI
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired

        return isReachable && !needsConnection
        */

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret

    }
}

/*
["status": 200, "data": {
    "access_token" = "f86983d6-ff8d-4b10-a8a7-13dc1818304a";
    canChangePassword = 1;
    email = "zeedeespeedy@gmail.com";
    emailVerified = 1;
    entityId = 1722;
    entityType = CUSTOMER;
    "expires_in" = 20477;
    firstName = ZeeDee;
    lastName = Records;
    message = "<null>";
    phoneNumber = 9009099685;
    phoneVerified = 1;
    "refresh_token" = "397e7917-58fd-4077-8c21-cfaecaab1e82";
    roleId = 2;
    roleName = CUSTOMER;
    scope = "trust read user_info write";
    status = 0;
    "token_type" = bearer;
    userId = 1947;
}, "message": User login successfully]
*/
