//
//  Utility.swift
//  appdele
//
//  Created by Danish computer technologies on 09/03/22.
//

import UIKit

class HElper{
  
    
   
    /// return validaton phone number
    static func isVAlidPhnNumber(phone: String) -> Bool{
       
            let phoneRegex = "^[0-9+]{0,1}+[0-9]{9,16}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return phoneTest.evaluate(with: phone)
        
    }
    
    /**
     Simple Alert
     - Show alert with title and alert message and basic one action
    */
    static func showSimpleAlert(vc:UIViewController,ttl:String,msg:String) {
        let alert = UIAlertController(title: ttl, message: msg, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Got it!", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
       
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func isValidEmailAddress(emailAddressString:String) -> Bool{
       
            
            var returnValue = true
            let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
            
            do {
                let regex = try NSRegularExpression(pattern: emailRegEx)
                let nsString = emailAddressString as NSString
                let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
                
                if results.count == 0
                {
                    returnValue = false
                }
                
            } catch let error as NSError {
                print("invalid regex: \(error.localizedDescription)")
                returnValue = false
            }
            
            return  returnValue
        }
    
    
    
}
