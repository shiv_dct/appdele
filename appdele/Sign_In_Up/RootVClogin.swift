//
//  RootVClogin.swift
//  appdele
//
//  Created by Danish computer technologies on 24/02/22.
//

import UIKit

class RootLoginVC:UIViewController {
   
    // MARK: - controller function
     var safeArea: UILayoutGuide!
    
     override func loadView() {
       super.loadView()
         view.backgroundColor = .secondarySystemBackground
         safeArea = view.layoutMarginsGuide
       
     }

    override func viewDidLoad() {
        super.viewDidLoad()
              setupTopView()
              setupmiddlelabels()
              setupBotomButtons()

    }
    
    
    func setupTopView() {
            
            view.addSubview(topImgV)
            
        topImgV.topAnchor.constraint(equalTo: safeArea.topAnchor,constant: 12).isActive = true
        topImgV.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        topImgV.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topImgV.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        }
    
    func setupmiddlelabels() {

//            let stackView = UIStackView(arrangedSubviews: [ labelOne,labelTwo])
//            stackView.axis = .vertical
//            stackView.spacing = 20
//            stackView.distribution = .fillProportionally
//            stackView.translatesAutoresizingMaskIntoConstraints = false
//            stackView.backgroundColor = .purple
//            //add stack view as subview to main view with AutoLayout
//            view.addSubview(stackView)
//            stackView.topAnchor.constraint(equalTo: topImgV.bottomAnchor, constant: 16).isActive = true
//            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
//            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
//            stackView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        view.addSubview(labelOne)
        view.addSubview(labelTwo)
        labelOne.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        labelOne.topAnchor.constraint(equalTo: topImgV.bottomAnchor, constant: 40).isActive = true
        labelTwo.centerXAnchor.constraint(equalTo: labelOne.centerXAnchor).isActive = true
        labelTwo.topAnchor.constraint(equalTo: labelOne.bottomAnchor, constant: 24).isActive = true
        labelTwo.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor,constant: 16).isActive = true
        labelTwo.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor,constant: -16).isActive = true
        
        let attributeString = NSMutableAttributedString(
               string: "Guest user",
               attributes: yourAttributes
            )
            guestUserButton.setAttributedTitle(attributeString, for: .normal)
        }
    func setupBotomButtons() {

            let stackView = UIStackView(arrangedSubviews: [ loginButton, registerButton, guestUserButton])
            stackView.axis = .vertical
            stackView.spacing = 12
       // stackView.backgroundColor = .orange
            stackView.distribution = .fillEqually
            stackView.translatesAutoresizingMaskIntoConstraints = false

            //add stack view as subview to main view with AutoLayout
            view.addSubview(stackView)
            stackView.topAnchor.constraint(equalTo: labelTwo.bottomAnchor, constant: 16).isActive = true
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
            stackView.heightAnchor.constraint(equalToConstant: 160).isActive = true

        }
    
    
    
    
    
    
    
    
    
    let topImgV:UIImageView = {
             let img = UIImageView()
        //img.backgroundColor = .red//Constants.Design.Color.Primary.v3Lightblue
             img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
             img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
            // img.layer.cornerRadius = 17.5
            img.image = UIImage(named: "deliveryboy")
             //img.clipsToBounds = true
       
            return img
         }()
    
    let labelOne:UILabel = {
            let label = UILabel()
        //label.backgroundColor = .green//Constants.Design.Color.Primary.v3txtfFontCol
       // label.font = Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "WelCome"
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18.0)
        label.numberOfLines = 1
        label.textAlignment = .center
       // label.sizeToFit()
            return label
    }()
    let labelTwo:UILabel = {
            let label = UILabel()
       // label.backgroundColor = .yellow//Constants.Design.Color.Primary.v3txtfFontCol
       // label.font = Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Need something from your favourite local store or want to send something to your friend? ZeeDeeSpeedy is here for all your daily needs."
        label.numberOfLines = 0
        label.font = UIFont(name: "AppleSDGothicNeo-SemiBold", size: 12.0)
        label.textColor = .gray
        label.sizeToFit()
        label.textAlignment = .center
    
            return label
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.darkText, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        button.addTarget(self, action: #selector(signin), for: .touchUpInside)
        return button
    }()
    let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Register", for: .normal)
        button.setTitleColor(.darkText, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)//UIFont.boldSystemFont(ofSize: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = UIColor.tertiarySystemBackground
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
         /*.font: UIFont.systemFont(ofSize: 14),*/
        .foregroundColor: UIColor.black,
         .underlineStyle: NSUnderlineStyle.single.rawValue
     ] // .double.rawValue, .thick.rawValue
    
    let guestUserButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Guest user", for: .normal)
        //button.setTitleColor(UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.layer.cornerRadius = 3
        //button.backgroundColor = UIColor.lightGray
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        
     
        
        return button
    }()
    
    @objc func register(){
        navigationController?.navigationBar.isHidden = true
        navigationController?.pushViewController(RegisterVc(), animated: true)
    }
    @objc func signin(){
        navigationController?.navigationBar.isHidden = true
        navigationController?.pushViewController(SignInVc(), animated: true)
    }
    @objc func handleSignUp() {
           validateForm()
       }
       
       func validateForm() {
//           guard let emailText = emailTextField.text, !emailText.isEmpty else { return }
//           guard let passwordText = passwordTextField.text, !passwordText.isEmpty else { return }
//           guard let usernameText = usernameTextField.text, !usernameText.isEmpty else { return }
//
//           startSigningUp(email: emailText, password: passwordText, username: usernameText)
           navigationController?.navigationBar.isHidden = true
           navigationController?.pushViewController(RootShiViewController(), animated: false)
       }
   
}
