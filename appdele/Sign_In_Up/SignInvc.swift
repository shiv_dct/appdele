//
//  SignInvc.swift
//  appdele
//
//  Created by Danish computer technologies on 04/03/22.
//

import UIKit





class SignInVc:UIViewController, UITextViewDelegate {
    var safeArea: UILayoutGuide!
   
    override func loadView() {
      super.loadView()
        view.backgroundColor = .secondarySystemBackground
        safeArea = view.layoutMarginsGuide
      
    }

   override func viewDidLoad() {
       super.viewDidLoad()
       setupview()
       let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
               view.addGestureRecognizer(tap)
   }
    var logintypeemail = true
    @objc func loginAction(){
        // check from email or mobile or facebook google or apple
        if logintypeemail{
            if HElper.isValidEmailAddress(emailAddressString: emailTextField.text!) && passwordTextField.text != ""{
                logInButton.isUserInteractionEnabled = false
                ApiCall.shared.login_email_User(email: emailTextField.text!, password: passwordTextField.text!) { sucess, ResponseJson in
                    if sucess{
                       // print(ResponseJson)
                        let userDefaults = UserDefaults.standard
                        let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: ResponseJson, requiringSecureCoding: false)
                        userDefaults.set(encodedData, forKey: "userDataemail")
                        userDefaults.set("emaillogin", forKey: "login")
                        DispatchQueue.main.async {
                            self.navigationController?.navigationBar.isHidden = true
                            self.navigationController?.pushViewController(RootShiViewController(), animated: false)
                        }
                    }else{
                        HElper.showSimpleAlert(vc: self, ttl: "something went wrong!", msg: "please try again!")
                    }
                    DispatchQueue.main.async {
                        self.logInButton.isUserInteractionEnabled = true}
                }
            }else{
                HElper.showSimpleAlert(vc: self, ttl: "Insufficient Information!", msg: "To login with Email, Please provide email address and password.")
            }
            
        }else{
            // otp mobile
        /*    {
                "message": "OTP is invalid or expired",
                "status": 400
            }
        https://service.doosy.in/doosy/user/login/customer/login/otp
            {"phoneNumber":"1234567890","otp":"123456"}
         */
            if HElper.isVAlidPhnNumber(phone: emailTextField.text!) && passwordTextField.text != ""{
                self.logInButton.isUserInteractionEnabled = false
                ApiCall.shared.login_mobile_User(mobile: emailTextField.text!, otp: passwordTextField.text!) { suces, response in
                    if suces{
                        let userDefaults = UserDefaults.standard
                        let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: false)
                        userDefaults.set(encodedData, forKey: "userDataOTP")
                        userDefaults.setValue("otplogin", forKey: "login")
                        DispatchQueue.main.async {
                           
                            self.navigationController?.navigationBar.isHidden = true
                            self.navigationController?.pushViewController(RootShiViewController(), animated: false)
                        }
                    }else{
                        HElper.showSimpleAlert(vc: self, ttl: "Error", msg: response["message"] as! String)
                    }
                    DispatchQueue.main.async {
                        self.logInButton.isUserInteractionEnabled = true }
                }

            }else{
              
                HElper.showSimpleAlert(vc: self, ttl: "Oops!", msg: "To login, you need to enter your valid mobile number and otp received on number")
            }
            
        }
                   

    }
    @objc func presentModalController() {
        let vc = CustomModalViewController()
        vc.modalPresentationStyle = .overCurrentContext
        // Keep animated value as false
        // Custom Modal presentation animation will be handled in VC itself
        self.present(vc, animated: false)
       
        //logInButton.isSelected = !logInButton.isSelected
       
        
       
    }
    @objc func changetitle(){
        
   /* https://service.doosy.in/doosy/user/login/customer/generate/otp/4563728274
    {
        "message": "Otp Generated Successfully",
        "status": 200
    }*/
        if HElper.isVAlidPhnNumber(phone: emailTextField.text!){
            self.otpButton.isUserInteractionEnabled = false
            ApiCall.shared.genrateOTPForNumber(number: emailTextField.text!) { suces, response in
                if suces{
                    DispatchQueue.main.async {
                        self.passwordTextField.keyboardType = .numberPad
                        self.passwordTextField.textAlignment = .center
                        self.numbertfH.constant = 30
                        self.otpButton.setTitle("Resend", for: .normal)
                    }
                }else{
                    HElper.showSimpleAlert(vc: self, ttl: "Error", msg: response["message"] as! String)
                }
                DispatchQueue.main.async {
                    self.otpButton.isUserInteractionEnabled = true}
            }

        }else{
          
            HElper.showSimpleAlert(vc: self, ttl: "Oops!", msg: "To get otp, you need to enter your valid mobile number first")
        }
       
    }
   
    func setupview(){
        view.addSubview(backButton)
        backButton.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        backButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        
        view.addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
       
        titleLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 50).isActive = true
        
        view.addSubview(otpview)
        otpview.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        otpview.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
        otpview.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16).isActive = true
        
        view.addSubview(textviewForgot)
        textviewForgot.topAnchor.constraint(equalTo: otpview.bottomAnchor, constant: 24).isActive = true
        textviewForgot.trailingAnchor.constraint(equalTo: otpview.trailingAnchor, constant: -8).isActive = true
        
        let attributedStringSign = NSMutableAttributedString(string: "Forgot Password")
        attributedStringSign.addAttribute(.link, value: "", range: NSRange(location: 0, length: 15))
        attributedStringSign.addAttributes([.underlineStyle:NSUnderlineStyle.single.rawValue,.font:UIFont(name: "AppleSDGothicNeo-Bold", size: 14.0) as Any], range: NSRange(location: 0, length: 15))

        //attributedStringSign.addAttributes([.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any,.foregroundColor:UIColor.gray], range: NSRange(location: 0, length: 24))
        textviewForgot.attributedText = attributedStringSign
        textviewForgot.delegate = self
        
        
        view.addSubview(textviewemail)
        textviewemail.topAnchor.constraint(equalTo: otpview.bottomAnchor, constant: 24).isActive = true
        textviewemail.leadingAnchor.constraint(equalTo: otpview.leadingAnchor,constant: 8).isActive = true
        let attributedString = NSMutableAttributedString(string: "Click Here for OTP  login")
attributedString.addAttribute(.link, value: "", range: NSRange(location: 0, length: 25))
attributedString.addAttributes([.underlineStyle:NSUnderlineStyle.single.rawValue,.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any], range: NSRange(location: 0, length: 25))

//attributedString.addAttributes([.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any,.foregroundColor:UIColor.gray], range: NSRange(location: 0, length: 40))
        textviewemail.attributedText = attributedString
        textviewemail.delegate = self
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        
        view.addSubview(logInButton)
        logInButton.topAnchor.constraint(equalTo: textviewForgot.bottomAnchor, constant: 24).isActive = true
        logInButton.centerXAnchor.constraint(equalTo: otpview.centerXAnchor).isActive = true
        logInButton.leadingAnchor.constraint(equalTo: otpview.leadingAnchor,constant: 16).isActive = true
        logInButton.trailingAnchor.constraint(equalTo: otpview.trailingAnchor, constant: -16).isActive = true
        otpview.addSubview(emailTextField)
        otpview.addSubview(otpButton)
        otpview.addSubview(passwordTextField)
        
        emailTextField.leadingAnchor.constraint(equalTo: otpview.leadingAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        emailTextField.trailingAnchor.constraint(equalTo: otpview.trailingAnchor).isActive = true
        emailTextField.topAnchor.constraint(equalTo: otpview.topAnchor, constant: 16).isActive = true
        
        otpButton.centerXAnchor.constraint(equalTo: otpview.centerXAnchor).isActive = true
        otpButton.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 16).isActive = true
        
        topanimatableheight = otpButton.heightAnchor.constraint(equalToConstant: 0)//.isActive = true
        topanimatableheight.isActive = true
        
        passwordTextField.leadingAnchor.constraint(equalTo: otpview.leadingAnchor).isActive = true
        numbertfH = passwordTextField.heightAnchor.constraint(equalToConstant: 36)
        passwordTextField.trailingAnchor.constraint(equalTo: otpview.trailingAnchor).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: otpButton.bottomAnchor, constant: 16).isActive = true
        passwordTextField.bottomAnchor.constraint(equalTo: otpview.bottomAnchor).isActive = true
        numbertfH.isActive = true
        
        view.addSubview(labelsocial)
        labelsocial.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        labelsocial.topAnchor.constraint(equalTo: logInButton.bottomAnchor, constant: 24).isActive = true
       
        
        view.addSubview(appleButton)
        view.addSubview(googleButton)
        view.addSubview(facebookButton)
        
        appleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        appleButton.topAnchor.constraint(equalTo: labelsocial.bottomAnchor, constant: 24).isActive = true
        appleButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        appleButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.28).isActive = true
        
        facebookButton.centerYAnchor.constraint(equalTo: appleButton.centerYAnchor).isActive = true
       
        facebookButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        facebookButton.widthAnchor.constraint(equalTo: appleButton.widthAnchor).isActive = true
        facebookButton.trailingAnchor.constraint(equalTo: appleButton.leadingAnchor, constant: -8).isActive = true
        
        googleButton.centerYAnchor.constraint(equalTo: appleButton.centerYAnchor).isActive = true
       
        googleButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        googleButton.widthAnchor.constraint(equalTo: appleButton.widthAnchor).isActive = true
        googleButton.leadingAnchor.constraint(equalTo: appleButton.trailingAnchor, constant: 8).isActive = true
        
        view.addSubview(textviewSignUp)
        textviewSignUp.topAnchor.constraint(equalTo: appleButton.bottomAnchor, constant: 24).isActive = true
        textviewSignUp.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        let attributedStringSignup = NSMutableAttributedString(string: "Don't have an acount? SIGN UP")
        attributedStringSignup.addAttribute(.link, value: "", range: NSRange(location: 22, length: 7))
        attributedStringSignup.addAttributes([.font:UIFont(name: "AppleSDGothicNeo-Bold", size: 14.0) as Any], range: NSRange(location: 22, length: 7))

        attributedStringSignup.addAttributes([.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any,.foregroundColor:UIColor.gray], range: NSRange(location: 0, length: 21))
        textviewSignUp.attributedText = attributedStringSignup
        textviewSignUp.delegate = self
        
        
    }
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
       // button.setTitle("back", for: .normal)
        //button.setTitleColor(.darkText, for: .normal)
       // button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.layer.cornerRadius = 8
       // button.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        button.addTarget(self, action: #selector(backtoview), for: .touchUpInside)
        button.setImage(UIImage(named: "back"), for: .normal)
        button.contentHorizontalAlignment = .left
        button.tintColor = .darkGray
        return button
    }()
    @objc func backtoview(){
        navigationController?.popToRootViewController(animated: true)//popViewController(animated: true)
    }
    let titleLabel: UILabel = {
            let label = UILabel()
            label.text = "Log In"
            label.numberOfLines = 0
            label.sizeToFit()
            label.textColor = UIColor.black
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18.0)
            return label
        }()
    
    let otpview:UIView = {
       let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        //v.backgroundColor = .red
        return v
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Email"
       // tf.isSecureTextEntry = true
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
       // tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
   
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Password"
        tf.isSecureTextEntry = true
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
       // tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
    let logInButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Log In", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .gray//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.isSelected = false
        button.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        return button
    }()
    let otpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("genrate otp", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
       // button.backgroundColor = .gray//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        button.alpha = 0
        button.addTarget(self, action: #selector(changetitle), for: .touchUpInside)
        return button
    }()
    var topanimatableheight:NSLayoutConstraint!
    var numbertfH:NSLayoutConstraint!
    
    let textviewemail:UITextView = {
        let textView = UITextView()
        textView.contentInsetAdjustmentBehavior = .never
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = NSTextAlignment.center
        //textView.textColor = UIColor.blue
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.tintColor = .gray//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        //textView.text = "Click Here for OTP login"
       
        return textView
    }()
    
    let textviewForgot:UITextView = {
        let textView = UITextView()
        textView.contentInsetAdjustmentBehavior = .never
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = NSTextAlignment.center
        textView.textColor = UIColor.blue
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.tintColor = .black//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // textView.text = "Forgot Password"
       // textView.backgroundColor = .red
        return textView
    }()
    
    let textviewSignUp:UITextView = {
        let textView = UITextView()
        textView.contentInsetAdjustmentBehavior = .never
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = NSTextAlignment.center
        textView.textColor = UIColor.blue
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.tintColor = .black//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        //textView.text = ""
       // textView.backgroundColor = .red
        return textView
    }()
    
    
    let googleButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("google", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .red//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    let facebookButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("facebook", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .blue//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    let appleButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("apple", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .black//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    
    let labelsocial:UILabel = {
            let label = UILabel()
       // label.backgroundColor = .yellow//Constants.Design.Color.Primary.v3txtfFontCol
       // label.font = Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "or Sign up via.."
        label.numberOfLines = 0
        label.font = UIFont(name: "AppleSDGothicNeo-SemiBold", size: 12.0)
        label.textColor = .gray
        label.sizeToFit()
        label.textAlignment = .center
    
            return label
    }()
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == textviewForgot{
            presentModalController()
            
        }else if textView == textviewSignUp{
            self.navigationController?.pushViewController(RegisterVc(), animated: true)
        }else{
            view.endEditing(true)
            emailTextField.text = nil
            passwordTextField.text = nil
            if topanimatableheight.constant != 0 {
                // set text as otp
                let attributedString = NSMutableAttributedString(string: "Click Here for OTP  login")
        attributedString.addAttribute(.link, value: "", range: NSRange(location: 0, length: 25))
        attributedString.addAttributes([.underlineStyle:NSUnderlineStyle.single.rawValue,.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any], range: NSRange(location: 0, length: 25))
                
      
                textviewemail.attributedText = attributedString
                UIView.animate(withDuration: 0.2) {
                    self.topanimatableheight.constant = 0
                    self.numbertfH.constant = 36
                    self.otpButton.alpha = 0
                }
                emailTextField.placeholder = "Email"
                emailTextField.keyboardType = .emailAddress
                logintypeemail = true
            }
            else{
                // email
                let attributedString = NSMutableAttributedString(string: "Click Here for Email login")
        attributedString.addAttribute(.link, value: "", range: NSRange(location: 0, length: 25))
        attributedString.addAttributes([.underlineStyle:NSUnderlineStyle.single.rawValue,.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any], range: NSRange(location: 0, length: 25))


     
                textviewemail.attributedText = attributedString
                UIView.animate(withDuration: 0.2) {
                    self.topanimatableheight.constant = 30
                    self.numbertfH.constant = 0
                    self.otpButton.setTitle("Genrate otp", for: .normal)
                    self.otpButton.alpha = 1
                }
                logintypeemail = false
                emailTextField.placeholder = "Phone"
                emailTextField.keyboardType = .numberPad
                
                
            }
            }
        return false
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {

        return false
    }
}
extension SignInVc: UITextFieldDelegate{
  
    @objc func handleTap() {
        view.endEditing(true)        }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        /*if textField == confirmPasswordTextField && confirmPasswordTextField.text! != passwordTextField.text!{
            HElper.showSimpleAlert(vc: self, ttl: "Error", msg: "Please check your password")
        }else if textField == passwordTextField && textField.text!.count < 8 {
            HElper.showSimpleAlert(vc: self, ttl: "Weak Password", msg: "Password should have 8 digit")
        }else if textField == emailTextField  && !HElper.isValidEmailAddress(emailAddressString: textField.text!){
            HElper.showSimpleAlert(vc: self, ttl: "Invalid Email", msg: "Please provide valid email id")
        }else if textField == phoneTextField && !HElper.isVAlidPhnNumber(phone: textField.text!){
            HElper.showSimpleAlert(vc: self, ttl: "Invalid Contact Number", msg: "Please Check your Contact number")
        }*/
            
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == phoneTextField || textField == passwordTextField || textField == confirmPasswordTextField {
//            textField.keyboardType = .numberPad
//        }else if textField == emailTextField{
//            textField.keyboardType = .emailAddress
//        }else{
//            textField.keyboardType = .asciiCapable
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
