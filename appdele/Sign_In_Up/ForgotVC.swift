//
//  ForgotVC.swift
//  appdele
//
//  Created by Danish computer technologies on 04/03/22.
//

import UIKit



class CustomModalViewController: UIViewController,UITextFieldDelegate{
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textfield.resignFirstResponder()
            animateContainerHeight(defaultHeight)
            return true
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.text = nil
            animateContainerHeight(maximumContainerHeight)
        }
    
    // define lazy views
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Forgot Password"
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 20.0)//.boldSystemFont(ofSize: 20)
        label.textColor = .black
        return label
    }()
    
    lazy var notesLabel: UILabel = {
        let label = UILabel()
        label.text = "Please enter your registered Email to receive password reset link"
        label.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)//.systemFont(ofSize: 16)
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()
    
    lazy var textfield:UITextField = {
            let tf = UITextField()
            tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
           // tf.textColor = Constants.Design.Color.Primary.v3txtfFontCol
            tf.placeholder = "Enter Email"
            //tf.backgroundColor = .green//Constants.Design.Color.Primary.v3superbackgroundCol
            tf.layer.cornerRadius = 10
            tf.delegate = self
            tf.borderStyle = UITextField.BorderStyle.roundedRect
            tf.clearsOnBeginEditing = true
            //tf.addlabel(str: "", w: 10)
            tf.keyboardType = .emailAddress
            tf.autocorrectionType = .no
            tf.translatesAutoresizingMaskIntoConstraints = false
            return tf
        }()
        // cancel button
        lazy var submitbtn:UIButton = {
            let b = UIButton(type: .custom)
            b.setTitle("Submit", for: .normal)
            b.setTitleColor(.black, for: .normal)
            b.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)//UIFont(name: "Helvetica", size: 16)
            b.translatesAutoresizingMaskIntoConstraints = false
            //b.setImage(UIImage(named: "Edit Square"), for: .normal)
            b.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            b.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
            //b.layer.borderWidth = 1.0
           // b.layer.borderColor = Constants.Design.Color.Primary.v3innerborder.cgColor
            b.layer.cornerRadius = 10
            b.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
            
           // b.clipsToBounds = true
            return b
        }()

    @objc func submitAction(){
        view.endEditing(true)
        /*
         https://service.doosy.in/doosy/user/login/forgotPassword
         
         POST
         {"email":"zeedeespeedy@gmail.com","userType":"CUSTOMER","type":"EMAIL","sendingType":"BOTH"}
         {
             "message": "Please refer the email and follow steps to reset your password.",
             "status": 200
         }
         */

        if HElper.isValidEmailAddress(emailAddressString: textfield.text!){
            self.submitbtn.isHidden = true
            ApiCall.shared.forgot_password(email: textfield.text!, userType: "CUSTOMER", type: "EMAIL", sendingType: "BOTH") { sucess, response in
                if sucess{
                    DispatchQueue.main.async {
                    self.notesLabel.text = response["message"] as? String
                    }
                }else{
                    DispatchQueue.main.async {
                        self.submitbtn.isHidden = false
                    }
                   
                }
            }
            
        }else{
            HElper.showSimpleAlert(vc: self, ttl: "Invalid email address", msg: "Please check your email address again!")
        }
        
    }
    lazy var contentStackView: UIStackView = {
        let spacer = UIView()
        //spacer.backgroundColor = .yellow
        let stackView = UIStackView(arrangedSubviews: [titleLabel, notesLabel,textfield, submitbtn, spacer])
        stackView.axis = .vertical
        stackView.spacing = 16.0
        return stackView
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 16
        view.clipsToBounds = true
        return view
    }()
    
    let maxDimmedAlpha: CGFloat = 0.6
    lazy var dimmedView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = maxDimmedAlpha
        return view
    }()
    
    // Constants
    let defaultHeight: CGFloat = 300
    let dismissibleHeight: CGFloat = 200
    let maximumContainerHeight: CGFloat = UIScreen.main.bounds.height - 164
    // keep current new height, initial is default height
    var currentContainerHeight: CGFloat = 300
    
    // Dynamic container constraint
    var containerViewHeightConstraint: NSLayoutConstraint?
    var containerViewBottomConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        // tap gesture on dimmed view to dismiss
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleCloseAction))
        dimmedView.addGestureRecognizer(tapGesture)
        
        setupPanGesture()
    }
    
    @objc func handleCloseAction() {
        animateDismissView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateShowDimmedView()
        animatePresentContainer()
    }
    
    func setupView() {
        view.backgroundColor = .clear
    }
    
    func setupConstraints() {
        // Add subviews
        view.addSubview(dimmedView)
        view.addSubview(containerView)
        dimmedView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(contentStackView)
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        
        // Set static constraints
        NSLayoutConstraint.activate([
            // set dimmedView edges to superview
            dimmedView.topAnchor.constraint(equalTo: view.topAnchor),
            dimmedView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            dimmedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            dimmedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            // set container static constraint (trailing & leading)
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            // content stackView
            contentStackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 32),
            contentStackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            contentStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            contentStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
        ])
        
        // Set dynamic constraints
        // First, set container to default height
        // after panning, the height can expand
        containerViewHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: defaultHeight)
        
        // By setting the height to default height, the container will be hide below the bottom anchor view
        // Later, will bring it up by set it to 0
        // set the constant to default height to bring it down again
        containerViewBottomConstraint = containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: defaultHeight)
        // Activate constraints
        containerViewHeightConstraint?.isActive = true
        containerViewBottomConstraint?.isActive = true
    }
    
    func setupPanGesture() {
        // add pan gesture recognizer to the view controller's view (the whole screen)
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(gesture:)))
        // change to false to immediately listen on gesture movement
        panGesture.delaysTouchesBegan = false
        panGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(panGesture)
    }
    
    // MARK: Pan gesture handler
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        // Drag to top will be minus value and vice versa
        print("Pan gesture y offset: \(translation.y)")
        
        // Get drag direction
        let isDraggingDown = translation.y > 0
        print("Dragging direction: \(isDraggingDown ? "going down" : "going up")")
        
        // New height is based on value of dragging plus current container height
        let newHeight = currentContainerHeight - translation.y
        
        // Handle based on gesture state
        switch gesture.state {
        case .changed:
            // This state will occur when user is dragging
            if newHeight < maximumContainerHeight {
                // Keep updating the height constraint
                containerViewHeightConstraint?.constant = newHeight
                // refresh layout
                view.layoutIfNeeded()
            }
        case .ended:
            // This happens when user stop drag,
            // so we will get the last height of container
            
            // Condition 1: If new height is below min, dismiss controller
            if newHeight < dismissibleHeight {
                self.animateDismissView()
            }
            else if newHeight < defaultHeight {
                // Condition 2: If new height is below default, animate back to default
                animateContainerHeight(defaultHeight)
            }
            else if newHeight < maximumContainerHeight && isDraggingDown {
                // Condition 3: If new height is below max and going down, set to default height
                animateContainerHeight(defaultHeight)
            }
            else if newHeight > defaultHeight && !isDraggingDown {
                // Condition 4: If new height is below max and going up, set to max height at top
                animateContainerHeight(maximumContainerHeight)
            }
        default:
            break
        }
    }
    
    func animateContainerHeight(_ height: CGFloat) {
        UIView.animate(withDuration: 0.4) {
            // Update container height
            self.containerViewHeightConstraint?.constant = height
            // Call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
        // Save current height
        currentContainerHeight = height
    }
    
    // MARK: Present and dismiss animation
    func animatePresentContainer() {
        // update bottom constraint in animation block
        UIView.animate(withDuration: 0.3) {
            self.containerViewBottomConstraint?.constant = 0
            // call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
    }
    
    func animateShowDimmedView() {
        dimmedView.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.dimmedView.alpha = self.maxDimmedAlpha
        }
    }
    
    func animateDismissView() {
        // hide blur view
        dimmedView.alpha = maxDimmedAlpha
        UIView.animate(withDuration: 0.4) {
            self.dimmedView.alpha = 0
        } completion: { _ in
            // once done, dismiss without animation
            self.dismiss(animated: false)
        }
        // hide main view by updating bottom constraint in animation block
        UIView.animate(withDuration: 0.3) {
            self.containerViewBottomConstraint?.constant = self.defaultHeight
            // call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
    }
}
