//
//  RegisterVC.swift
//  appdele
//
//  Created by Danish computer technologies on 04/03/22.
//




/*
 https://service.doosy.in/doosy/customer
 POST
 {"firstName":"test","lastName":"user","phoneNumber":"1234567890","email":"testuser@gmail.com","password":"12345678","confirmPassword":"12345678","acceptTnC":true,"registeredVia":"APP","active":true}
 {
     "message": "Same phone number exist for different customer!! Kindly use different number.",
     "status": 400
 }
 
 
 */

import UIKit

class RegisterVc:UIViewController, UITextViewDelegate {
    var safeArea: UILayoutGuide!
   
    @objc func register(){
        view.endEditing(true)
        // check all field are not empty
        if nameTextField.text!.isEmpty {
            HElper.showSimpleAlert(vc: self, ttl: "Name Required", msg: "Please provide name and other fields to perform registration")
        }else if lastnameTextField.text!.isEmpty{
            HElper.showSimpleAlert(vc: self, ttl: "Last Name Required", msg: "Please provide last name and other fields to perform registration")
        }
        else if emailTextField.text!.isEmpty {
            HElper.showSimpleAlert(vc: self, ttl: "Email Required", msg: "Please provide email and other fields to perform registration")
        }
        else if phoneTextField.text!.isEmpty{
            HElper.showSimpleAlert(vc: self, ttl: "Contact Number Required", msg: "Please provide name and other fields to perform registration")
        }
        else if confirmPasswordTextField.text!.isEmpty || passwordTextField.text!.isEmpty {
            HElper.showSimpleAlert(vc: self, ttl: "Set Password", msg: "Please Set password and confirm password to perform registration")
        }
        else if !toggleButton.isSelected{
            HElper.showSimpleAlert(vc: self, ttl: "Aceept Term & Conditions", msg: "Read and Aceept Term & Condition befor proceed")
        }else{
            registerUser()
        }

    }
    
    func registerUser(){
        ApiCall.shared.register_new_User(firstName: nameTextField.text!, lastName: lastnameTextField.text!, phoneNumber: phoneTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, confirmPassword: confirmPasswordTextField.text!, registeredVia: "APP", acceptTnC: true, active: true) { sucess, response in
            
            if sucess{
               // print(response)//["active": 1, "phoneNumber": 4563728274, "createdAt": 2022-03-09T09:23:41.852+00:00, "email": tryh@rjf.com, "gender": <null>, "registeredVia": APP, "name": Test Tr, "emailVerified": 0, "birthDate": <null>, "firstName": Test, "id": 1831, "walletAmt": 0, "status": Pending, "phoneVerified": 0, "lastName": Tr, "addressList": <null>, "userId": 2181]
                // handle result here
                // https://cocoacasts.com/ud-4-how-to-store-a-dictionary-in-user-defaults-in-swift
                
                let userDefaults = UserDefaults.standard
                let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: false)
                userDefaults.set(encodedData, forKey: "userData")
                userDefaults.setValue("register", forKey: "login")
                //userDefaults.object(forKey: "myKey")
                DispatchQueue.main.async {
                    self.navigationController?.navigationBar.isHidden = true
                    self.navigationController?.pushViewController(RootShiViewController(), animated: false)
                }
                
            }else{
                //print(response)
                guard let msg = response["message"] as? String else{
                    return
                }
                HElper.showSimpleAlert(vc: self, ttl: "Status", msg: msg)
            }
        }
    }
    
    
    
    override func loadView() {
      super.loadView()
        view.backgroundColor = .secondarySystemBackground
        safeArea = view.layoutMarginsGuide
      
    }

   override func viewDidLoad() {
       super.viewDidLoad()
       
       setupScrollView()
      // setupViews()
       setuptextfield()
//       setuplabel()
//       setupbuttons()
       let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
               view.addGestureRecognizer(tap)
   }
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "First Name"
        //tf.isSecureTextEntry = true
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
       
        //UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
    
    let lastnameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Last Name"
        //tf.isSecureTextEntry = true
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
        //tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
    let phoneTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Contact No."
        tf.keyboardType = .namePhonePad
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
        //tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Email"
        tf.keyboardType = .emailAddress
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
       // tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Set Password"
        tf.isSecureTextEntry = true
        tf.keyboardType = .namePhonePad
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
       // tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
    let confirmPasswordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Re-Enter Password"
        //tf.isSecureTextEntry = true
        tf.keyboardType = .namePhonePad
        tf.borderStyle = .roundedRect
        tf.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0)
        tf.backgroundColor = .clear
       // tf.backgroundColor = UIColor(white: 0, alpha: 0.1)
       // tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
        
    }()
   
    func setuptextfield(){
        contentView.addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
       
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        contentView.addSubview(nameTextField)
        nameTextField.delegate = self
        nameTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        nameTextField.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.47).isActive = true
        nameTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        contentView.addSubview(lastnameTextField)
        lastnameTextField.delegate = self
        lastnameTextField.leadingAnchor.constraint(equalTo: nameTextField.trailingAnchor, constant: 8).isActive = true
        lastnameTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        lastnameTextField.topAnchor.constraint(equalTo: nameTextField.topAnchor).isActive = true
        lastnameTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        contentView.addSubview(phoneTextField)
        phoneTextField.delegate = self
        phoneTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        phoneTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        phoneTextField.topAnchor.constraint(equalTo: lastnameTextField.bottomAnchor, constant: 8).isActive = true
        phoneTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        contentView.addSubview(emailTextField)
        emailTextField.delegate = self
        emailTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        emailTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        emailTextField.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 8).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        contentView.addSubview(passwordTextField)
        passwordTextField.delegate = self
        passwordTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 8).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        contentView.addSubview(confirmPasswordTextField)
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        confirmPasswordTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        confirmPasswordTextField.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 8).isActive = true
        confirmPasswordTextField.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        
        contentView.addSubview(toggleButton)
        contentView.addSubview(textvtNc)
        textvtNc.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 64).isActive = true
        textvtNc.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -16).isActive = true
        textvtNc.topAnchor.constraint(equalTo: confirmPasswordTextField.bottomAnchor, constant: 8).isActive = true
        let attributedString = NSMutableAttributedString(string: "By clicking on Sign up you are agree to term & conditions")
attributedString.addAttribute(.link, value: "http://www.zeedeespeedy.com/#/company/terms-and-condition", range: NSRange(location: 40, length: 17))
attributedString.addAttributes([.underlineStyle:NSUnderlineStyle.single.rawValue,.font:UIFont(name: "AppleSDGothicNeo-Bold", size: 14.0) as Any], range: NSRange(location: 40, length: 17))

attributedString.addAttributes([.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any,.foregroundColor:UIColor.gray], range: NSRange(location: 0, length: 40))
        textvtNc.attributedText = attributedString
        textvtNc.delegate = self
        
        toggleButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        toggleButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        toggleButton.centerYAnchor.constraint(equalTo: textvtNc.centerYAnchor).isActive = true
        toggleButton.trailingAnchor.constraint(equalTo: textvtNc.leadingAnchor,constant: -19).isActive = true
        
        
        contentView.addSubview(registerButton)
        registerButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        registerButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        registerButton.topAnchor.constraint(equalTo: textvtNc.bottomAnchor,constant: 16).isActive = true
        contentView.addSubview(labelsocial)
        labelsocial.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelsocial.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 16).isActive = true
       
        
        contentView.addSubview(appleButton)
        contentView.addSubview(googleButton)
        contentView.addSubview(facebookButton)
        
        appleButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        appleButton.topAnchor.constraint(equalTo: labelsocial.bottomAnchor, constant: 16).isActive = true
        appleButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        appleButton.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.28).isActive = true
        
        facebookButton.centerYAnchor.constraint(equalTo: appleButton.centerYAnchor).isActive = true
       
        facebookButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        facebookButton.widthAnchor.constraint(equalTo: appleButton.widthAnchor).isActive = true
        facebookButton.trailingAnchor.constraint(equalTo: appleButton.leadingAnchor, constant: -8).isActive = true
        
        googleButton.centerYAnchor.constraint(equalTo: appleButton.centerYAnchor).isActive = true
       
        googleButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        googleButton.widthAnchor.constraint(equalTo: appleButton.widthAnchor).isActive = true
        googleButton.leadingAnchor.constraint(equalTo: appleButton.trailingAnchor, constant: 8).isActive = true
        
        contentView.addSubview(textvSignup)
        textvSignup.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        textvSignup.topAnchor.constraint(equalTo: appleButton.bottomAnchor, constant: 16).isActive = true
        let attributedStringSign = NSMutableAttributedString(string: "Allready have an acount! SIGN IN")
        attributedStringSign.addAttribute(.link, value: "", range: NSRange(location: 24, length: 8))
        attributedStringSign.addAttributes([.underlineStyle:NSUnderlineStyle.single.rawValue,.font:UIFont(name: "AppleSDGothicNeo-Bold", size: 14.0) as Any], range: NSRange(location: 25, length: 7))

        attributedStringSign.addAttributes([.font:UIFont(name: "AppleSDGothicNeo-Regular", size: 14.0) as Any,.foregroundColor:UIColor.gray], range: NSRange(location: 0, length: 24))
        textvSignup.attributedText = attributedStringSign
        textvSignup.delegate = self
        
       // textvSignup.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
       // textvSignup.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
       
        
        
        appleButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -156).isActive = true
        
        
    }
//    let yourAttributes: [NSAttributedString.Key: Any] = [
//         /*.font: UIFont.systemFont(ofSize: 14),*/
//        .foregroundColor: UIColor.black,
//         .underlineStyle: NSUnderlineStyle.single.rawValue
//     ]

    let labelsocial:UILabel = {
            let label = UILabel()
       // label.backgroundColor = .yellow//Constants.Design.Color.Primary.v3txtfFontCol
       // label.font = Constants.Design.Font.v3SBMAN12//UIFont.boldSystemFont(ofSize: 20)
       // label.textColor =  Constants.Design.Color.Primary.v3txtfFontCol//Constants.Design.Color.Primary.v3txtfFontCol//colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "or Sign up via.."
        label.numberOfLines = 0
        label.font = UIFont(name: "AppleSDGothicNeo-SemiBold", size: 12.0)
        label.textColor = .gray
        label.sizeToFit()
        label.textAlignment = .center
    
            return label
    }()
    
    let textvtNc:UITextView = {
        let textView = UITextView()
        textView.contentInsetAdjustmentBehavior = .never
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = NSTextAlignment.justified
        textView.textColor = UIColor.blue
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.tintColor = UIColor.black//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        
        return textView
    }()
    
    let textvSignup:UITextView = {
        let textView = UITextView()
        textView.contentInsetAdjustmentBehavior = .never
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = NSTextAlignment.center
        textView.textColor = UIColor.blue
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.tintColor = .black//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        
        return textView
    }()
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == textvtNc{
            UIApplication.shared.open(URL)}
        else if textView == textvSignup{
                //navigationController?.popViewController(animated: true)
            self.navigationController?.pushViewController(SignInVc(), animated: true)
            }
        return false
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {

        return false
    }
    
    func setupbuttons(){
        
    }
    
    let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("SIGN UP", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .gray//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        button.addTarget(self, action: #selector(register), for: .touchUpInside)
        return button
    }()
    let toggleButton: UIButton = {
        let button = UIButton(type: .custom)
        //button.setTitle("Login", for: .normal)
        //button.setTitleColor(.darkText, for: .normal)
        //button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 4
        button.tintColor = .clear
        button.backgroundColor = .clear//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        button.setImage(UIImage(named: "uncheck"), for: .normal)
        button.setImage(UIImage(named: "check"), for: .selected)
        button.isSelected = false
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.borderWidth = 1.0

        button.addTarget(self, action: #selector(toggleaction), for: .touchUpInside)
        return button
    }()
    @objc func toggleaction(){
        view.endEditing(true)
        toggleButton.isSelected = !toggleButton.isSelected
    }
    let googleButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("google", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .red//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    let facebookButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("facebook", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .blue//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    let appleButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("apple", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 8
        button.backgroundColor = .black//UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
       // button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    let backButton: UIButton = {
        let button = UIButton(type: .system)
       // button.setTitle("back", for: .normal)
        //button.setTitleColor(.darkText, for: .normal)
       // button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16.0)
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.layer.cornerRadius = 8
       // button.backgroundColor = UIColor(red: 192.0/255.0, green: 255.0/255.0, blue: 0, alpha: 1)
        button.addTarget(self, action: #selector(backtoview), for: .touchUpInside)
        button.setImage(UIImage(named: "back"), for: .normal)
        button.contentHorizontalAlignment = .left
        button.tintColor = .darkGray
        return button
    }()
    @objc func backtoview(){
        navigationController?.popToRootViewController(animated: true)//popViewController(animated: true)
    }
    let scrollView = UIScrollView()
    let contentView = UIView()
    func setupScrollView(){
       // scrollView.backgroundColor = .red
        //contentView.backgroundColor = .blue
           scrollView.translatesAutoresizingMaskIntoConstraints = false
           contentView.translatesAutoresizingMaskIntoConstraints = false
           view.addSubview(scrollView)
       
           scrollView.addSubview(contentView)
           
           scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
           scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
           scrollView.topAnchor.constraint(equalTo: safeArea.topAnchor,constant: 50).isActive = true
           scrollView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
           
           contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
           contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
           contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
           contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        view.addSubview(backButton)
        backButton.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        backButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
       }
    
    let titleLabel: UILabel = {
            let label = UILabel()
            label.text = "Sign Up"
            label.numberOfLines = 0
            label.sizeToFit()
            label.textColor = UIColor.black
            label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18.0)
            return label
        }()
       
}

extension RegisterVc: UITextFieldDelegate{
  
    @objc func handleTap() {
        view.endEditing(true)        }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField == confirmPasswordTextField && confirmPasswordTextField.text! != passwordTextField.text!{
            HElper.showSimpleAlert(vc: self, ttl: "Error", msg: "Please check your password")
        }else if textField == passwordTextField && textField.text!.count < 8 {
            HElper.showSimpleAlert(vc: self, ttl: "Weak Password", msg: "Password should have 8 digit")
        }else if textField == emailTextField  && !HElper.isValidEmailAddress(emailAddressString: textField.text!){
            HElper.showSimpleAlert(vc: self, ttl: "Invalid Email", msg: "Please provide valid email id")
        }else if textField == phoneTextField && !HElper.isVAlidPhnNumber(phone: textField.text!){
            HElper.showSimpleAlert(vc: self, ttl: "Invalid Contact Number", msg: "Please Check your Contact number")
        }
            
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneTextField || textField == passwordTextField || textField == confirmPasswordTextField {
            textField.keyboardType = .numberPad
        }else if textField == emailTextField{
            textField.keyboardType = .emailAddress
        }else{
            textField.keyboardType = .asciiCapable
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
